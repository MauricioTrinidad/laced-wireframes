$(document).ready(function () {
    var $body = $('body'),
            form_result = $('.contact-form-container .form-result'),
            form_markup = $('.contact-form-container form, .contact .initial-copy');

    var sendEmail = function (form) {
        $.ajax({
            type: "POST",
            url: '/email.php',
            data: $(form).serialize(),
            dataType: "json"
        }).done(function (data) {
            if (data.result === 'SUCCESS') {
                var msg = 'Thank you for contacting us! One of our team members will be contacting you within the next ' +
                    '24hours or less to get you started.<br><br>' +
                    'In a rush? Emergency? Just call our office directly at (310) 316-1962 - ' +
                    'and we will get you in touch with someone immediately to help you & your team.<br><br>' +
                    'Thank you again for contacting LACED Agency!';

                form_result.html(msg);
                form_result.removeClass('fail');
                $(form)[0].reset();
                form_markup.hide();
            } else {
                form_result.html('An error ocurred when try to send the email');
                form_result.addClass('fail');
            }
            form_result.show();

        }).fail(function (data) {
            form_result.html('An error ocurred when try to send the email');
            form_result.addClass('fail');
            form_result.show();
        });

    };

    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL = {
            setupFormValidation: function () {
                //form validation rules
                $("#contact-form").validate({
                    rules: {
                        name: "required",
                        company: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        phone: "required",
                        tell_us: "required"
                    },
                    messages: {
                        name: "Please enter your Name",
                        company: "Please enter your Company",
                        email: {
                            required: "Please enter your Email Address",
                            email: "Please enter a valid email address"
                        },
                        phone: "Please enter your Phone Number",
                        tell_us: "Field required"
                    },
                    submitHandler: function (form) {
                        form_result.hide();
                        sendEmail(form);
                        return false;
                    }
                });
            }
        };

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
});