$(document).ready(function () {
    var $body = $('body'),
            form_result = $('.form-wrapper .form-result-quick');

    $('#nav-icon1, #nav-icon2,#nav-icon3, #nav-icon4').click(function () {
        $(this).toggleClass('open');
        $body.toggleClass('open');
        $body.removeClass('open-form');
    });

    $('.quick-info').on('click', function (event) {
        event.preventDefault();
        $body.addClass('open');
        $body.addClass('open-form');
        $('#nav-icon1, #nav-icon2, #nav-icon3, #nav-icon4').addClass('open');
    });

    var $w = $(window).scroll(function () {
        if ($w.scrollTop() > 50 && !$body.hasClass('open')) {
            $body.addClass('on-scroll-change');
        } else {
            $body.removeClass('on-scroll-change');
        }
    });

    $(window).resize(function () {
        if ($(window).width() >= 1280) {
            console.log('bigger than 1280');
            $('#nav-icon1, #nav-icon2,#nav-icon3, #nav-icon4, body').removeClass('open');
            $('body').removeClass('open-form');
        }
    });

    var sendEmailProjectForm = function (form) {
        $.ajax({
            type: "POST",
            url: '/email.php',
            data: $(form).serialize(),
            dataType: "json"
        }).done(function (data) {
            if (data.result === 'SUCCESS') {
                form_result.html('The email has been sent successfully!');
                form_result.removeClass('fail');
                $(form)[0].reset();
            } else {
                form_result.html('An error ocurred when try to send the email');
                form_result.addClass('fail');
            }
            form_result.show();

        }).fail(function (data) {
            form_result.html('An error ocurred when try to send the email');
            form_result.addClass('fail');
            form_result.show();
        });

    };

    (function ($, W, D) {
        var JQUERY4U = {};

        JQUERY4U.UTIL = {
            setupFormValidation: function () {
                //form validation rules
                $("#project-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        phone: "required",
                        project_brief: "required"
                    },
                    messages: {
                        name: "Please enter your Name",
                        email: {
                            required: "Please enter your Email Address",
                            email: "Please enter a valid email address"
                        },
                        phone: "Please enter your Phone number",
                        project_brief: "Please enter your Project Brief"
                    },
                    submitHandler: function (form) {
                        form_result.hide();
                        sendEmailProjectForm(form);
                        return false;
                    }
                });
            }
        };

        //when the dom has loaded setup form validation rules
        $(D).ready(function ($) {
            JQUERY4U.UTIL.setupFormValidation();
        });

    })(jQuery, window, document);
});