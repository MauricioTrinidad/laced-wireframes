/* global YT*/

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var playerReady = false;
var players = [];
function onYouTubeIframeAPIReady() {
    playerReady = true;
}

function showPlayerStatus(parent) {
    parent.find('> picture').hide();
    parent.find('.video-wrapper').removeClass('hide');
    parent.find('.youtube-player__close-btn').removeClass('youtube-player--show-close-btn');
}

function closePlayerStatus(parent) {
    parent.find('> picture').show();
    parent.find('.video-wrapper').addClass('hide');
    parent.find('.youtube-player__close-btn').removeClass('youtube-player--show-close-btn');
}

$(document).ready(function () {
    $('.play-button').on('click', function (event) {
        event.preventDefault();
        var videoId = $(this).data('video');
        var n = $(this).data('n');        
        var playerId = 'player' + n;
        var playerDOM = $('<div id="' + playerId + '" class="player"></div>');
        var parent = $(this).parent('.player-wrapper');
        parent.find('.video-wrapper').append(playerDOM);
        if (!players[n]) {
            players[n] = new YT.Player(playerId, {
                height: '360',
                width: '640',
                videoId: videoId,
                playerVars: {
                    controls: 2,
                    disablekb: true,
                    rel: false,
                    showinfo: false,
                    modestbranding: 1,
                    iv_load_policy: 1
                },
                events: {
                    'onReady': function (event) {
                        console.log('player ready');
                        event.target.playVideo();
                        showPlayerStatus(parent);
                    },
                    'onStateChange': function (event) {
                        if (event.data === YT.PlayerState.PLAYING) {
                            parent.find('.play-button').hide();
                            parent.find('.youtube-player__close-btn').removeClass('youtube-player--show-close-btn');
                        } else if (event.data === YT.PlayerState.PAUSED) {
                            parent.find('.play-button').show();
                            parent.find('.youtube-player__close-btn').addClass('youtube-player--show-close-btn');
                        }
                    }
                }
            });
        } else {
            showPlayerStatus(parent);
            players[n].playVideo();
        }

        parent.find('.youtube-player__close-btn').on('click', function (event) {
            event.preventDefault();
            closePlayerStatus(parent);
        });
    });
});