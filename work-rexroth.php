<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" 
                    srcset="images/Desktop-1920x1080_1x-Rexroth-HeroBanner-GFX.jpg,
                    images/Desktop-2880x1620-2x-Rexroth-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)" 
                    srcset="images/Tablet-lg-1280x720-1x-Rexroth-HeroBanner-GFX.jpg,
                    images/Tablet-lg-1920x1080-2x-Rexroth-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)" 
                    srcset="images/Tablet-sm-768x432-1x-Rexroth-HeroBanner-GFX.jpg,
                    images/Tablet-sm-1152x648-2x-Rexroth-HeroBanner-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x552-1x-Rexroth-HeroBanner-GFX.jpg,
                    images/Mobile-621x828-2x-Rexroth-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>The Challenge</h2>
          </div>
          <p>Rexroth came to LACED agency to create a digital catalog that would combine 8 different print catalogs’ products. Rexroth knew they needed a digital solution that would not only lower the hard costs of print catalogs, but also provide a tool for their sales teams, and a more engaging product experience for customers.</p>
        </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080_1x_Bosch-HowTo-Video1-GFX.jpg">
            <source media="" srcset="images/Youtube-621x349-2x_Bosch-HowTo-Video1-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="Jf7In2YHTN8">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>The Solution</h2>
          </div>
          <p>LACED was definitely up to the challenge. The methodology behind the iPAD app is X, Y, Z — or 3Dimensial axis thinking. Using a 3D visual approach, LACED created an opening sequence video, to introduce users to the functionality of the iPAD app in an entertaining way. We used motion design, and morphed the final sequence into the visual navigation of the iPAD app. Staying within the strict brand guidelines and maintaining brand consistency with Rexroth’s pre-existing marcom, LACED organized over 3000 products into an easy-to-use iPAD app, that was rolled out live by the end of January 2012.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/414-px-960x648-1x-Rexroth-5Stars-Image1-GFX.jpg, images/414-px-1152x648-2x-Rexroth-5Stars-Image1-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x233-1x-Rexroth-5Stars-Image1-GFX.jpg, images/Mobile-621x349-2x-Rexroth-5Stars-Image1-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>"Everyone who has seen the app so far is truly impressed by the functional quality and flawless design."</h2>
            <h3>— Almir Tucek</h3>
          </div>
        </div>
      </section>

      <section class="detail-section clearfix">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>The Results</h2>
          </div>
          <ul>
            <li>5 ★★★★★ App Rating</li>
            <li>Significantly Reduced Print Catalog Costs</li>
            <li>Increases in Overall Sales for Q1-2012</li>
            <li>Favorable Customer & Distributor Feedback</li>
            <li>Easy product updates with Seamless Integration to back-end CMS</li>
          </ul>

      </section>

      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>