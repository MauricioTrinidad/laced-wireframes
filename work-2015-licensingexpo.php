<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/2015-licensing-expo/Desktop_1440x810_1x_100w_2015-licensing-expo.jpg, images/2015-licensing-expo/Desktop_2880x1620_2x_100w_2015-licensing-expo.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x438_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x552_1x_DremelDepot-Hero-GFX.jpg, images/Mobile-621x828-2x-WorkDetailHeroGFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>About Client:</h2>
          </div>
          <p>UBM Advanstar, part of UBM Americas, is a US-based event and marketing services business serving the Fashion, Licensing, Automotive and Powersports industries. The company owns and operates a portfolio of 50 tradeshows, 5 publications, and over 30 electronic products and websites. Among their properties is The Licensing Expo, the largest international gathering of licensing brands, properties, agents, manufacturers, and distribution channels in the world. The show hosts over 90+ different countries and over 5000+ brands.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/414-px-960x540_1x_DremelDepot-Segment-Image1-GFX.jpg,
                    images/414-px-1152x648_2x_DremelDepot-Segment-Image1-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233_1x_DremelDepot-Segment-Image1-GFX.jpg,
                    images/Mobile-621x349_2x_DremelDepot-Segment-Image1-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>The Licensing Expo 2015<br />June 9 - June 11<br />Pre-Day Seminars kick off on Monday, June 8</h2>
        </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>The Challenge:</h2>
            <p>UBM asked LACED Agency’s Social Team to help their team build “Buzz” and promote the 2015 Licensing Expo by creating an integrated social media campaign, promoting brand awareness for “The Licensing Expo” brand in the social media space, and driving registrations for the largest international licensing show in the world.</p>
          </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/Desktop-1600x900_1x_DremelDepot-Process-Image2-GFX.jpg, images/Desktop-2160x1215_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Process-Image2-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Pre-Show Anticipation: 30 Days before Show</h2>
          </div>
          <h3>Content Marketing</h3>
          <p>The LACED Social Media team Implemented a complete 30-Day Content Marketing Campaign across UBM’s social channels (Twitter, Facebook, YouTube, & Instagram) for “The Licensing Expo” brand - ensuring heavy promotion of hashtag “#Licensing15” for the 2015 Show. This included custom creative visuals and clever content; ranging from multiple daily posts, aggregated content, outreach & connection with influencers, and more. Content areas focused on educational, show guidance, best practices for networking, how to secure new deals, top events to attend, amazing keynote & panels not to miss, teased celebrity surprise appearances & announcements, as well as what apps to use for optimizing meeting times throughout the show week.</p>
          <h3>Social Media Contest</h3>
          <p>The LACED Social Media team also created & managed a social media contest that was implemented in 2 Waves for maximum exposure and promotional buzz – prior to show.  Prizes included Opening Night Tickets to see 5th Harmony perform while enjoying complimentary drinks & appetizers and mingling with the top licensing talent & retailers in the world.</p>
          <h3>Expanded - Digital Video Spots</h3>
          <p>The LACED Video team was asked to create several Digital Video Spots including a ‘Buzz Video” in an effort to drive increased interest & sow registrations in the weeks leading up to the 2016 Licensing Expo in Vegas.</p>
          <ul>
          	<li>(13) Thirteen Total Video Spots </li>
	<li>2016 Buzz Video </li>
	<li>1min+ version</li>
	<li>:30sec version - Played to open KeyNote Address on Day 1</li>
	<li>:15sec version </li>
	<li>“Why Attend Licensing Expo?” - Digital Video Spot</li>
	<li>“What to Expect” -  Digital Video Spot</li>
	<li>(8) Eight Mini-Testimonial Versions - Digital Video Spots</li>
	<li>“Countdown” Video - Digital Video Spot</li>
</ul>
        </div>
      </section>

      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/Desktop-1600x900_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Desktop-2160x1215_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Live Show Coverage: 4 Days On-The-Ground Social Team</h2>
          </div>
          <h3>Show Social Media Coverage</h3>
          <p>On-the-ground social teams utilizing Twitter, Facebook, Instagram for 4-Days of show coverage; documenting keynotes, special events, to celebrities and red carpet appearances, to educational sessions & licensing panels, to award announcements, to special brand reveals, to exhibitor booths & attendee connections, to the MatchMaking Service Center interactions - and every special moment in-between.</p>
          <h3>Opening Night Party Coverage</h3>
          <p>For the Opening Night Show coverage - the LACED Social Media team utilizes Twitter’s new live stream video service “Periscope” (which had just launched two months prior on Mar 26, 2015) in order to connect viewers at home with the excitement of The Licensing Expo’s Opening Night Celebrations with 5th Harmony performing live. During this event alone - Periscope helped drive more than 608.8K impressions in a single day on Twitter for The Licensing Expo (average for the 34-Day Campaign was 44.9K impressions per day) with hundreds of users tuning into the event to watch live video coverage.</p>
        </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080_1x_DremelDepot-HowTo-Video1-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x_DremelDepot-HowTo-Video1-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="Tl9eu7KO9xg">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>H2 Headline</h2>
          </div>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/414-px-960x540_1x_DremelDepot-Countdown-Image4-GFX.jpg, images/414-px-1152x648_2x_DremelDepot-Countdown-Image4-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Countdown-Image4-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Countdown-Image4-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h4><i>“We made an excellent decision when we hired the Social & Video teams at LACED Agency to manage our social media campaign for the 2015 Licensing Expo. LACED Agency went above and beyond to ensure that every aspect of the campaign, from SM content creation to video production, was top-notch quality and always in-line with our strategic initiatives. I’m looking forward to working with LACED again in the future and would recommend them to anyone in need of help with social media and digital marketing.”</i></h4>
            <h3>- Becca Dawson</h3>
            <h4>Marketing Manager, UBM - June 2015</h4>
        </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>H2 Headline</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
          </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" 
                    srcset="images/414-px-960x540_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/414-px-1152x648_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/Mobile-621x349_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper no-padding-bottom">
          <div>
            <h4><i>LACED Agency wanted to make DIY “fun” again – so providing incentives for increased, on-going community engagement was a must! We designed a rewards program with fun DIY Badges based on skill level, level of engagement, and posts/shares to encourage on-going engagement tied to monthly promotions and rewards!</i></h4>
        </div>
        </div>
      </section>


      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>