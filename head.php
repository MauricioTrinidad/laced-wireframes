<head>
  <title>LACED Agency</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="cropped-512x512_LACED_icon-1-32x32.png" sizes="32x32">
  <link rel="icon" href="cropped-512x512_LACED_icon-1-192x192.png" sizes="192x192">
  <link rel="apple-touch-icon-precomposed" href="cropped-512x512_LACED_icon-1-180x180.png">
  <meta name="msapplication-TileImage" content="cropped-512x512_LACED_icon-1-270x270.png">
  <link rel="stylesheet" type="text/css" href="/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="/css/main.css">
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
</head>