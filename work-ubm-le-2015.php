<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/work-ubm-le-2015/Desktop-1920x1080-1x-UBM-LE-2015Event-HeroBanner-GFX.jpg, 
                    images/work-ubm-le-2015/Desktop-2880x1620-2x-UBM-LE-2015Event-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/work-ubm-le-2015/Tablet-lg-1280x720-1x-UBM-LE-2015Event-HeroBanner-GFX.jpg, 
                    images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-HeroBanner-GFX.jpg, 
                    images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-HeroBanner-GFX.jpg 2x">
            <source media="" srcset="images/work-ubm-le-2015/Mobile-414x552-1x-UBM-LE-2015Event-HeroBanner-GFX.jpg, 
                    images/work-ubm-le-2015/Mobile-621x828-2x-UBM-LE-2015Event-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>About Client</h2>
          </div>
          <p>UBM Advanstar, part of UBM Americas, is a US-based event and marketing services business serving the Fashion, Licensing, Automotive and Powersports industries. The company owns and operates a portfolio of 50 tradeshows, 5 publications, and over 30 electronic products and websites. Among their properties is The Licensing Expo, the largest international gathering of licensing brands, properties, agents, manufacturers, and distribution channels in the world. The show hosts over 90+ different countries and over 5000+ brands.</p>
        </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/work-ubm-le-2015/Desktop-1600x900-1x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg,
                  images/work-ubm-le-2015/Desktop-2160x1215-2x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/work-ubm-le-2015/Tablet-lg-1280x720-1x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg 2x">
          <source media=""
                  srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg,
                  images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-PreShow-Image1-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Agency Video Work</h2>
          </div>
          <p>LACED Agency was hired to encourage and drive overall show attendance, build greater brand awareness, and create excitement. A key component of the overall agency strategy for <i>The Licensing Expo Campaign</i> was to create a <i>Digital Video Commercial Series</i> designed to visually promote the show for both prospective and returning attendees.</p>
        </div>
      </section>

      <!-- SECTION 50/50 with QUOTE -->
      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/work-ubm-le-2015/Desktop-960x540-1x-UBM-LE-2015Event-SocialContest-Image-GFX.jpg,
                    images/work-ubm-le-2015/Desktop-1152x648-2x-UBM-LE-2015Event-SocialContest-Image-GFX.jpg 2x">
            <source media=""
                    srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-SocialContest-Image-GFX.jpg,
                    images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-SocialContest-Image-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper" style="background-color:#eaeaea;">
          <div>
            <h4><i>"The LACED Agency team produced eye-catching, engaging content for Licensing Expo leading up to the show and also providing on-site coverage for four full days. They were troopers from 7 AM to midnight almost every day, never missing an important event to cover at the show and treating our exhibitors and attendees with the utmost respect. Our social media engagement skyrocketed in the month leading up to and during the show, thanks to the great coverage and the unique content they produced. The Digital Video Commercial Series was key in the overall campaign's success - and was a HUGE hit among attendees."</i></h2>
              <h3>— Emilie Schwab</h3>
              <h4>Marketing Manager At UBM</h4>
          </div>
        </div>
      </section>


      <!-- SECTION VIDEO 1 -->
      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/work-ubm-le-2015/Desktop-1920x1080-1x-UBM-LE-2015Event-Video-CoverGFX.jpg">
            <source media="" srcset="images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-Video-CoverGFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="kNJHO3OtDjA">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>2016 Licensing Expo - 1+ Minute Sizzle/Promo Video</h2>
          </div>
          <p>The 1st of three in the Video Series; this video was designed to be longer in length, providing the most in depth overview of the show (highlighting business categories, international countries attending, events, & overall excitement). The Video was rolled out to social media channels, advertising & media placements, email marketing, and web channels – 2 months prior to the show.</p>
        </div>
      </section>

      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/work-ubm-le-2015/Desktop-1600x900-1x-UBM-LE-2015Event-Crowd-GFX.jpg,
                  images/work-ubm-le-2015/Desktop-2160x1215-2x-UBM-LE-2015Event-Crowd-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/work-ubm-le-2015/Tablet-lg-1280x720-1x-UBM-LE-2015Event-Crowd-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-Crowd-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-Crowd-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-Crowd-GFX.jpg 2x">
          <source media=""
                  srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-Crowd-GFX.jpg,
                  images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-Crowd-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Agency Video Work</h2>
          </div>
          <p>LACED Agency was hired to encourage and drive overall show attendance, build greater brand awareness, and create excitement. A key component of the overall agency strategy for <i>The Licensing Expo Campaign</i> was to create a <i>Digital Video Commercial Series</i> designed to visually promote the show for both prospective and returning attendees.</p>
        </div>
      </section>
      
      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/work-ubm-le-2015/Desktop-1600x900-1x-UBM-LE-2015Event-Social-Phones-GFX.jpg,
                  images/work-ubm-le-2015/Desktop-2160x1215-2x-UBM-LE-2015Event-Social-Phones-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/work-ubm-le-2015/Tablet-lg-1280x72-1x-UBM-LE-2015Event-Social-Phones-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-Social-Phones-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-Social-Phones-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-Social-Phones-GFX.jpg 2x">
          <source media=""
                  srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-Social-Phones-GFX.jpg,
                  images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-Social-Phones-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </section>
      
      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/work-ubm-le-2015/Desktop-960x540-1x-UBM-LE-2015Event-Batmobile-GFX.jpg,
                    images/work-ubm-le-2015/Desktop-1152x648-2x-UBM-LE-2015Event-Batmobile-GFX.jpg 2x">
            <source media=""
                    srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-Batmobile-GFX.jpg,
                    images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-Batmobile-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper" style="background-color:#eaeaea;">
          <div>
            <h4><i>"The LACED Agency team produced eye-catching, engaging content for Licensing Expo leading up to the show and also providing on-site coverage for four full days. They were troopers from 7 AM to midnight almost every day, never missing an important event to cover at the show and treating our exhibitors and attendees with the utmost respect. Our social media engagement skyrocketed in the month leading up to and during the show, thanks to the great coverage and the unique content they produced. The Digital Video Commercial Series was key in the overall campaign's success - and was a HUGE hit among attendees."</i></h2>
              <h3>— Emilie Schwab</h3>
              <h4>Marketing Manager At UBM</h4>
          </div>
        </div>
      </section>
      
      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>Ongoing Marketing: Community Engagement</h2>
            <p>Communication is paramount to any successful relationship – offline or online, business or personal. Building a digital community demands sophisticated communication functionality and LACED delivered. The MESSAGES box alerts users to notifications; when users “like” or “comment” on a project of theirs, as well as Notifications from Dremel regarding new promotions or earning reward badges. Users also receive personal emails when they engage that community; joining receives a “welcome email”, various community behaviors receive “Congrats! You have earned___new badge!”, etc… This not only helps keep users engagement with DremelDepot but also drove excitement for users.</p>
          </div>
      </section>
      
      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/work-ubm-le-2015/Desktop-1600x900-1x-UBM-LE-2015Event-5h-GFX.jpg,
                  images/work-ubm-le-2015/Desktop-2160x1215-2x-UBM-LE-2015Event-5h-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/work-ubm-le-2015/Tablet-lg-1280x72-1x-UBM-LE-2015Event-5h-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-5h-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-5h-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-5h-GFX.jpg 2x">
          <source media=""
                  srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-5h-GFX.jpg,
                  images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-5h-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Agency Video Work</h2>
          </div>
          <p>LACED Agency was hired to encourage and drive overall show attendance, build greater brand awareness, and create excitement. A key component of the overall agency strategy for <i>The Licensing Expo Campaign</i> was to create a <i>Digital Video Commercial Series</i> designed to visually promote the show for both prospective and returning attendees.</p>
        </div>
      </section>
      
      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/work-ubm-le-2015/Desktop-1600x900-1x-UBM-LE-2015Event-quote-GFX.jpg,
                  images/work-ubm-le-2015/Desktop-2160x1215-2x-UBM-LE-2015Event-quote-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/work-ubm-le-2015/Tablet-lg-1280x72-1x-UBM-LE-2015Event-quote-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-lg-1920x1080-2x-UBM-LE-2015Event-quote-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/work-ubm-le-2015/Tablet-sm-768x432-1x-UBM-LE-2015Event-quote-GFX.jpg,
                  images/work-ubm-le-2015/Tablet-sm-1152x648-2x-UBM-LE-2015Event-quote-GFX.jpg 2x">
          <source media=""
                  srcset="images/work-ubm-le-2015/Mobile-414x233-1x-UBM-LE-2015Event-quote-GFX.jpg,
                  images/work-ubm-le-2015/Mobile-621x349-2x-UBM-LE-2015Event-quote-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Agency Video Work</h2>
          </div>
          <p>LACED Agency was hired to encourage and drive overall show attendance, build greater brand awareness, and create excitement. A key component of the overall agency strategy for <i>The Licensing Expo Campaign</i> was to create a <i>Digital Video Commercial Series</i> designed to visually promote the show for both prospective and returning attendees.</p>
        </div>
      </section>

      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>