<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/Desktop-1920x1080_1x_DremelDepot-Hero-GFX.jpg, images/Desktop-2880x1620_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x438_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x552_1x_DremelDepot-Hero-GFX.jpg, images/Mobile-621x828-2x-WorkDetailHeroGFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>A Community Built For DIYers</h2>
          </div>
          <p>Dremel wanted to strengthen their longstanding partnership with HomeDepot, while simultaneously adding value to their customers’ day-to-day lives. Creating a digital community where DIYers (Do-It-Yourselfers) and DFMers (Do-It-For-Me-ers) alike, could connect with each other, share, learn, and inspire each other was just the thing Dremel needed to connect their customers and partner, HomeDepot—all together in one place, digitally.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/414-px-960x540_1x_DremelDepot-Segment-Image1-GFX.jpg,
                    images/414-px-1152x648_2x_DremelDepot-Segment-Image1-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233_1x_DremelDepot-Segment-Image1-GFX.jpg,
                    images/Mobile-621x349_2x_DremelDepot-Segment-Image1-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>Targeting 3 DIY Segments; The Crafter, The Fixer, and The Builder – LACED Agency created DremelDepot.com, a facebook-esque digital & social application that connects the world’s most talented & novice DIYers in one community.</h2></div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>The Process</h2>
            <p>From drafted sketch to a successful digital application – LACED Agency created DremelDepot’s community to visually appeal to both DIYers and DFMers; populating the site with content like DIY Trends, How-To’s, Dremel Expert Advice, etc..–content that users not only care about but is easy to access and understand. We implemented a fun 5-step assessment system, to help users determine what level of DIYer they were, and filter information to them based on their interests and skill levels. We architected DremelDepot.com as a multi-functional social community; personal pages to highlight individual user projects, a project gallery to browse the latest talent and find inspiration, ability to comment and “like” other projects connecting users, learn from and engage Dremel Experts, access to the latest tips & tricks for common DIY challenges, new Dremel promotions and product releases, as well as well as access to their own personalized, digital toolbox to track both current and new tool purchases from HomeDepot – all in one community. Not to mention, LACED Agency merged both HomeDepot and Dremel’s brand guidelines to produce a sleek, clean, and beautifully partnered brand consistent design that both brands could brag about.</p>
          </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/Desktop-1600x900_1x_DremelDepot-Process-Image2-GFX.jpg, images/Desktop-2160x1215_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Process-Image2-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Making It Responsive</h2>
          </div>
          <p>DIYers are on the go; in the yard, on the job site, or the shed – and that means mobile! LACED Agency designed the DremelDepot.com community to be responsive; meaning accessible on the desktop, mobile, and tablet devices. So when ever or where ever you are – Dremel Depot stays with you. The mobile site is as rich in functionality and features as the desktop version and provides the same interactive DIY Community experience. Users can easily like, comment, and see the number of views on any project, navigate with an easy menu bar, search for Projects, view the Project Gallery, upload a project of their own, view their messages, and visit their accounts….all in the palm of your hand. DremelDepot.com Connecting the experienced and emerging DIYers together in one community – accessible on all your devices, all the time.</p>
        </div>
      </section>

      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/Desktop-1600x900_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Desktop-2160x1215_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Responsive-Image3-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Responsive-Image3-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Marketing</h2>
            <h3>The Video</h3>
          </div>
          <p>LACED created two (2) marketing videos for DremelDepot.com. The first video was a teaser, designed to drive membership and build pre-launch buzz. The second video was longer in length, used fun “How-To” visuals, and a voiceover. This video was designed to entertain and educate (or as we like to say, “Edutain”) users about the community’s features and overall functionality.</p>
        </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080_1x_DremelDepot-HowTo-Video1-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x_DremelDepot-HowTo-Video1-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="Tl9eu7KO9xg">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>Pre-Launch: Anticipation</h2>
          </div>
          <p>In order to create a pre-launch buzz, LACED Agency recommended a count-down page and sign-up form for early adopters. Meanwhile, Exclusive Invites were sent out to top DIY personalities, DIY bloggers and media, as well as key influencers to test the community site in a Phased Rollout Strategy. Gathering their initial experiences, ratings, and feedback was key to future marketing efforts and future phased rollouts to wider DIY audiences. Allowing these DIY Influencers to “Share their DremelDepot experience” with their personal audiences also created not only virility but also reputability, authenticity, and transparency for DremelDepot as the community expanded to wider audiences.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/414-px-960x540_1x_DremelDepot-Countdown-Image4-GFX.jpg, images/414-px-1152x648_2x_DremelDepot-Countdown-Image4-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Countdown-Image4-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Countdown-Image4-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>Dremeldepot’s Pre-launch Marketing efforts included a landing page, quick sign-up form for those users who were interested in getting an early invite to the community or being the first to try it out once the formal launch rolled out.</h2></div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>Ongoing Marketing: Community Engagement</h2>
            <p>Communication is paramount to any successful relationship – offline or online, business or personal. Building a digital community demands sophisticated communication functionality and LACED delivered. The MESSAGES box alerts users to notifications; when users “like” or “comment” on a project of theirs, as well as Notifications from Dremel regarding new promotions or earning reward badges. Users also receive personal emails when they engage that community; joining receives a “welcome email”, various community behaviors receive “Congrats! You have earned___new badge!”, etc… This not only helps keep users engagement with DremelDepot but also drove excitement for users.</p>
          </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" 
                    srcset="images/414-px-960x540_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/414-px-1152x648_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/Mobile-621x349_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper no-padding-bottom">
          <div>
            <h2>LACED Agency wanted to make DIY “fun” again – so providing incentives for increased, on-going community engagement was a must! We designed a rewards program with fun DIY Badges based on skill level, level of engagement, and posts/shares to encourage on-going engagement tied to monthly promotions and rewards!</h2></div>
        </div>
      </section>


      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>