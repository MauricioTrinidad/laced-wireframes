<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail careers">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/Desktop-1920x1080_1x_DremelDepot-Hero-GFX.jpg, images/Desktop-2880x1620_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x438_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x552_1x_DremelDepot-Hero-GFX.jpg, images/Mobile-621x828-2x-WorkDetailHeroGFX.jpg 2x">
            <img>
          </picture>
        </div>
      </section>
      
      <div class="title-section">
        <h1>CAREERS</h1>
      </div>
      
      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top">
          <div class="no-margin-bottom">
            <p>From drafted sketch to a successful digital application – LACED Agency created DremelDepot’s community to visually appeal to both DIYers and DFMers; populating the site with content like DIY Trends, How-To’s, Dremel Expert Advice, etc..–content that users not only care about but is easy to access and understand. We implemented a fun 5-step assessment system, to help users determine what level of DIYer they were, and filter information to them based on their interests and skill levels. We architected DremelDepot.com.</p>
          </div>
      </section>
      
      <?php include 'careers-partial.php'; ?>

      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>