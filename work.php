<!DOCTYPE html>
<html>
  <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section overview">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" 
                    srcset="images/Desktop-1980x1080-1x-Work-Overview-Hero-GFX.jpg,
                    images/Desktop-2880x1620-2x-Work-Overview-Hero-GFX.jpg 2x">
            <source media="(min-width: 769px)" 
                    srcset="images/Tablet-lg-1280x720-1x-Work-Overview-Hero-GFX.jpg,
                    images/Tablet-lg-1920x1080-2x-Work-Overview-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" 
                    srcset="images/Tablet-sm-768x432-1x-Work-Overview-Hero-GFX.jpg,
                    images/Tablet-sm-1152x648-2x-Work-Overview-Hero-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x55-1x-Work-Overview-Hero-GFX.jpg,
                    images/Mobile-621x828-2x-Work-Overview-Hero-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="hero-content"><h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas orci nibh</h2>
        <a class="more-details main" href="#">VIEW SUCCESS STORY</a></div>
      </section> 
      <div class="sections clearfix">
        <?php include 'more-work.php'; ?>
      </div>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>