<footer class="contact-container">
  <div class="contact-content">
    <div class="footer-logo">
      <div class="logo-wrapper">
        <img src="images/98x98-2x-Laced-Icon.png">
      </div>
    </div>
    <address>
      <p>
        LACED Agency<br>
        114 S Catalina Avenue, Suite 103<br>
        Redondo Beach, CA 90277
      </p>
      <p>Call 310-316-1962</p>
      <p>Email <a href="mailto: contact@lacedagency.com">contact@lacedagency.com</a></p>
    </address>
    <ul class="menu-social-links clearfix">
      <li><a href="https://www.facebook.com/LACEDagency/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
      <li><a href="https://twitter.com/lacedtweet" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="https://www.linkedin.com/company/laced-interactive" target="_blank"><i class="fa fa fa-linkedin-square" aria-hidden="true"></i></a></li>
      <li><a href="https://www.youtube.com/user/lacedinteractive" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
    </ul>
  </div>
</footer>