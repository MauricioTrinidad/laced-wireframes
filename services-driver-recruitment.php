<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/services-dr/Desktop-1920x1080-1x-driver-recruitment-hero.jpg, 
                    images/services-dr/Desktop-2880x1620-2x-driver-recruitment-hero.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/services-dr/Tablet-lg-1280x720-1x-driver-recruitment-hero.jpg, 
                    images/services-dr/Tablet-lg-1920x1080-2x-driver-recruitment-hero.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/services-dr/Tablet-sm-768x432-1x-driver-recruitment-hero.jpg, 
                    images/services-dr/Tablet-sm-1152x648-2x-driver-recruitment-hero.jpg 2x">
            <source media="" srcset="images/services-dr/Mobile-414x552-1x-driver-recruitment-hero.jpg, 
                    images/services-dr/Mobile-621x828-2x-driver-recruitment-hero.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>LACED Drives Your Recruitment.<br>Bottom line.</h2>
          </div>
          <p>Ready for a BETTER QUALITY LEAD that converts quickly? Well, that’s exactly what you can expect when you partner with LACED Agency for Driver Recruitment & Media Services. Delivering a full suite of Strategic AOR Solutions your team can expect better quality leads at a lower CPL (Cost Per Lead), a high volume of leads in targeted areas you need now, better conversions at a lower CPH (Cost Per Hire) which means savings to your bottom line, and superior reporting that’s easy to understand, visual, fast, and clearly demonstrates incremental improvements that you can see with data. Better Driver Recruitment & Retention is here.  Are you ready for BETTER? We are.</p>
        </div>
      </section>

      <!--four cols section-->
      <section class="full">
        <div class="clearfix">
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box">
            <img class="icon" src="images/services-dr/icon-media-management-black.png">
            <div class="heading"><h2>Media Management</h2></div>
            <p class="caption">Expect our best team, working collaboratively with top media partners in a focused, strategic, data-driven, and accountable manner to allow your media and your budget to start working better for you.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box">
            <img class="icon" src="images/services-dr/icon-data-analysis-black.png">
            <div class="heading"><h2>Data Analysis</h2></div>
            <p class="caption">Without resources to accurately decipher data; Data-driven programs fall flat. Our agency analysts can help you make sense of your data with real-time insights, strategic direction & recommendations.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box">
            <img class="icon" src="images/services-dr/icon-creative-black.png">
            <div class="heading"><h2>Creative</h2></div>
            <p class="caption">Creative that works, understands the pulse of your drivers. Expect a flow of fresh ideas, visual design that entices, and a message that resonates on every platform, every channel, every time.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box">
            <img class="icon" src="images/services-dr/icon-technical-black.png">
            <div class="heading"><h2>Technical</h2></div>
            <p class="caption">Technology is the cornerstone of a successful digital media campaign. Our team of engineers build solid foundations and framework to support your media campaigns scale and scope – whatever the size.</p>
            <a class="more-details">Learn More</a>
          </div>
        </div>
      </section>

      <section class="detail-section with-icon with-contrast service-mm-icon">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Media Management</h2>
            <p>Expect our “A” team on your campaigns, working collaboratively with top media partners in a focused, strategic, data-driven, and accountable manner to allow your media and your budget to start working better for you.</p>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/services-dr/Desktop-1600x900-1x-driver-recruitment-media-management.jpg, 
                  images/services-dr/Desktop-2160x1215-2x-driver-recruitment-media-management.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/services-dr/Tablet-lg-1280x720-1x-driver-recruitment-media-management.jpg, 
                  images/services-dr/Tablet-lg-1920x1080-2x-driver-recruitment-media-management.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/services-dr/Tablet-sm-768x432-1x-driver-recruitment-media-management.jpg, 
                  images/services-dr/Tablet-sm-1152x648-2x-driver-recruitment-media-management.jpg 2x">
          <source media="" srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-media-management.jpg, 
                  images/services-dr/Mobile-621x349-2x-driver-recruitment-media-management.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Planning & Strategy</h2>
          </div>
          <p>It’s easy to come up with new ideas in the planning phase; the hard part is letting go of what worked for you two years ago, and will soon be out of date. After all, planning is bringing the future into the present so that you can do something about it now.</p>
          <p>If you need high lead volume every month, we will get you there without sacrificing the quality of those leads – ensuring they convert and stay. If you need a low CPH (Cost Per Hire) every quarter but also a high volume of daily, targeted, regional leads in key areas with a low CPL (Cost Per Lead) each month, each quarter, and long-term improved retention numbers – we can get you there too. And that’s the real balancing act; ensuring the short-term strategy and planning works for each quarter while simultaneously moving the team forward in a direction that meets the long-tail goals for the organization annually. In this area, our agency team excels.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-planning-strategy.jpg, 
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-planning-strategy.jpg 2x">
            <source media="" srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-planning-strategy.jpg, 
                    images/services-dr/Mobile-621x349-2x-driver-recruitment-planning-strategy.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h4><i>Our team of strategists believe a good plan is like a road map: it shows the final destination and the best way to get there.</i></h4>
            <h3>— Michael Walsh<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Founder | CEO</h3>
        </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Media Buying</h2>
            <p>Our team will bring you into the exciting world of the media buying process, educating you on what’s new and cool in the market, collaborating with you on what’s best to achieve your goals, and recommending the most savvy ways to get cost-effective results, fast. From the latest in programmatic media buying to advanced targeting, to hybrid search and recruitment technologies with real-time media delivery – our media team stays on top of the latest. You can expect added value with every media buy.</p>
            <p>LACED Agency’s media team methodology entitled “Incremental Improvements Approach” – is not only data-driven, cost effective, and strategic – it also ensures transparency in planning, buying, and measurement every step of the way. You will know how your investments are being spent, if the purchase has real value and why/why not, the probability for success and expected timeframe for results – delivering media buying that is not only more effective but more efficient.</p>
          </div>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-media-buying.jpg,
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-media-buying.jpg 2x">
            <source media="" 
                    srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-media-buying.jpg,
                    images/services-dr/Mobile-621x349-2x-driver-recruitment-media-buying.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h4><i>Be Where Your Audience Is, When They Are Ready To Act.</i></h4>
        </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Media Relationships</h2>
            <p>Today’s media landscape requires media partners who are not only trust-worthy but accessible, fair, transparent, fast on their feet, responsive, and open to new ideas and fast turn-arounds. We have been building strong media relationships for years, and our media partners are some of the best in the business! We pull everyone together as a team to get invested in your campaign – encouraging the best performance possible each month. Regular communication and scheduled meetings ensure goals are met, hurdles are addressed and cleared, reports reviewed and analyzed, ideas exchanged, and every new pitch is heard. With competitive buying power, our media relationships ensure your advertising is displayed in a positive, consistent, and credible manner – one that gets results. Still not convinced? See what our media partners have to say about us…</p>
          </div>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-media-relationships.jpg,
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-media-relationships.jpg 2x">
            <source media="" 
                    srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-media-relationships.jpg,
                    images/services-dr/Mobile-621x349-2x-driver-recruitment-media-relationships.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h4><i>"I feel confident in your agency and would love to see more clients gravitate to LACED. You and your crew do an excellent job and you know your stuff!"</i></h4>
            <h3>— Top Secret Media Partner</h3>
        </div>
        </div>
      </section>

      <!--red four cols-->
      <section class="full">
        <div class="clearfix">
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box quote">
            <span class="quote-symbol">&#10077;</span>
            <blockquote>“LACED Agency’s detailed approach & reporting are second to none. They are so easy to work with! We’ve had a lot of success on the campaigns they manage, and we very much hope to work on other client campaigns together with them in the future.”</blockquote>
            <div class="heading"><h2>Media Partner</h2></div>
            <p class="caption">Confidential</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box quote">
            <span class="quote-symbol">&#10077;</span>
            <blockquote>"Thank God for LACED Agency - they are the hardest working, attention to detail, and commitment to quality agency I've ever seen. And honestly, the results speak for themselves. Our best campaigns are with their group. It is an absolute pleasure working with them."</blockquote>
            <div class="heading"><h2>Media Partner</h2></div>
            <p class="caption">Confidential</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box quote">
            <span class="quote-symbol">&#10077;</span>
            <blockquote>"In my 20+ years in the ad agency biz (prior to moving to digital media), I have never seen an agency more dedicated to their client's campaigns than LACED. They are fun, easy to work with, have great reporting, and they are fair. A good agency is hard to find, and LACED is it!"</blockquote>
            <div class="heading"><h2>Media Partner</h2></div>
            <p class="caption">Confidential</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box quote">
            <span class="quote-symbol">&#10077;</span>
            <blockquote>We have appreciated the expertise you have shared with us to make the campaigns better, and the confidence you gave our team to work harder and grow the business the way you have. It is our pleasure working with LACED."</blockquote>
            <div class="heading"><h2>Media Partner</h2></div>
            <p class="caption">Confidential</p>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Media Reporting</h2>
            <p>Data-driven programs are just data if you don’t have the time, team, or resources in place to accurately decipher the data. Reporting is great too – but to determine what to do next you need real insights that only come from pro-level analysis of your data. That’s where we come in.</p>
            <p>Throw those spreadsheets away! Our media reporting is not only data-driven, timely, robust, visual, and easy to understand – but we have agency analysts ready to help you make sense of it. We schedule regular meetings to review what’s working and what’s not, decipher the data, determine strategic direction, discuss collaboratively with you, and then provide real-time recommendations for achieving incremental improvements.</p>
          </div>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-media-reporting.jpg,
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-media-reporting.jpg 2x">
            <source media="" 
                    srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-media-reporting.jpg,
                    images/services-dr/Mobile-621x349-1x-driver-recruitment-media-reporting.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h4><i>What’s Reporting Without Analysis? LACED has a team of analysists ready to work with your specific needs.</i></h4>
        </div>
        </div>
      </section>

      <section class="detail-section with-icon with-contrast service-creative-icon">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Creative Services</h2>
            <p>Seamless Integration, Support, & Security.</p>
          </div>
        </div>
      </section>

      <!--four white cols-->
      <section class="full">
        <div class="clearfix">
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Display Banners</h2></div>
            <p class="caption">Beautifully Branded, Visually Engaging, Seamless ATS Integration, Advanced Tracking, High CTR’s.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Landing Pages</h2></div>
            <p class="caption">Responsive & Dynamic, Beautifully Branded, Visually Engaging, Seamless ATS Integration, Click To Call, Scalability for large volume goals & new campaigns in a matter of hours vs. weeks.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Technical</h2></div>
            <p class="caption">Targeted HTML Email Blasts are one of many key tactics in a large-scale, complex media campaign. Responsive-Designed, Beautifully Branded, Visually Engaging, Seamless ATS Integration, Advanced Tracking, High CTR’s.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Mobile</h2></div>
            <p class="caption">From Click to Call features, to quick form features – expect LACED Agency to deliver a “mobile first” strategy on all creative reaching the fastest growing audience of job seekers.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Copywriting</h2></div>
            <p class="caption">Strategic internal agency processes ensure your campaign copy is always fresh, in line with contextual and voice search trends, is delivered and resonates with your key targets.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Digital Video</h2></div>
            <p class="caption">Engaging, Innovative, Inspiring – our award-wining video team delivers short form spots that drive results.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>XML</h2></div>
            <p class="caption">One size does not fit all. LACED approaches each XML feed individually to ensure the highest performance for your recruitment needs, taking into account each media platforms intricacies and utilizing them to drive higher CTRs for each campaign.</p>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Print</h2></div>
            <p class="caption">When properly executed, offline channels can still deliver.</p>
          </div>
        </div>
      </section>

      <section class="detail-section with-icon with-contrast service-tech-icon">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Technical services</h2>
            <p>From drafted sketch to a successful digital application – LACED Agency created DremelDepot’s community to visually appeal to both DIYers and DFMers; populating the site with content like DIY Trends, How-To’s, Dremel Expert Advice, etc.</p>
          </div>
        </div>
      </section>

      <!--four white cols-->
      <!--four white cols-->
      <section class="full">
        <div class="clearfix">
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Scalability</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>System Framework</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Software Automation</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Server Maintenance</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Seamless ATS Integration</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Programming</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Development</h2></div>
          </div>
          <div class="item-wrapper col-sm-2 col-lg-4 border info-box vcenter">
            <div class="heading"><h2>Coding</h2></div>
          </div>
        </div>
      </section>

      <section class="detail-section with-icon with-contrast">
        <div class="detail-content-wrapper">
          <div class="no-margin-bottom">
            <h2>Experience Better!</h2>
            <p>We’re passionate about what we do & we’d love to work with you! 
Hire LACED Agency and experience better…</p>
          </div>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-experience-better-A.jpg,
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-experience-better-A.jpg 2x">
            <source media="" 
                    srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-experience-better-A.jpg,
                    images/services-dr/Mobile-621x349-2x-driver-recruitment-experience-better-A.jpg 2x">
            <img class="img-responsive">
          </picture>
          <div class="inner no-padding-tablet-desktop right-border">
            <div>
              <h2>Lower CPL (Cost Per Lead)</h2></div>
            <p>On your media buys</p>
            <div>
              <h2>Lower CPH (Cost Per Hire)</h2></div>
            <p>On your recruitment advertising</p>
            <div>
              <h2>Increased Retention</h2></div>
            <p>Great hires, making YOU their permanent home</p>
            <div>
              <h2>Easy Agency Transition</h2></div>
            <p>Expect a transparent agency process that allows you to decide how hands on/hands off you want to be</p>
            <div>
              <h2>Competitive Pricing / Value</h2></div>
            <p>Get more from your budget!</p>
          </div>
        </div>
        <div class="left">
          <picture class="content">
            <source media="(min-width: 769px)" 
                    srcset="images/services-dr/Desktop-960x540-1x-driver-recruitment-experience-better-B.jpg,
                    images/services-dr/Desktop-1152x648-2x-driver-recruitment-experience-better-B.jpg 2x">
            <source media="" 
                    srcset="images/services-dr/Mobile-414x233-1x-driver-recruitment-experience-better-B.jpg,
                    images/services-dr/Mobile-621x349-2x-driver-recruitment-experience-better-B.jpg 2x">
            <img class="img-responsive">
          </picture>
          <div class="inner no-padding-bottom">
            <div>
              <h2>A Passionate Team</h2></div>
            <p>Bringing fresh ideas and industry relevant media news to you every quarter allowing you and your team to stay ahead of the competition</p>
            <div>
              <h2>Visual Design & Messaging</h2></div>
            <p>That resonates & builds your brand</p>
            <div>
              <h2>Seamless ATS Integration for Trucking Clients</h2></div>
            <p>Our technical team has you covered from A to Z</p>
            <div>
              <h2>Peace of Mind</h2></div>
            <p>Knowing you have a strategic partner in your corner</p>
          </div>
        </div>
      </section>

      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>