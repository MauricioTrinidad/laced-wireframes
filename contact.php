<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail contact">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)"
                    srcset="images/contact/Desktop-1920x1080_1x_contact-hero.jpg, 
                    images/contact/Desktop-2880x1620_2x_contact-hero.jpg 2x">
            <source media="(min-width: 769px)"
                    srcset="images/contact/Tablet-lg-1280x720_1x_contact-hero.jpg, 
                    images/contact/Tablet-lg-1920x1080_2x_contact-hero.jpg 2x">
            <source media="(min-width: 431px)"
                    srcset="images/contact/Tablet-sm-768x438_1x_contact-hero.jpg, 
                    images/contact/Tablet-sm-1152x648_2x_contact-hero.jpg 2x">
            <source media=""
                    srcset="images/contact/Mobile-414x552_1x_contact-hero.jpg, 
                    images/contact/Mobile-621x828-2x-contact-hero.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="title-section"><h1>CONTACT</h1></div>
        <div class="main no-padding-bottom">
          <div class="initial-copy">
            <div>
              <h2>We would love to hear from you!</h2>
            </div>
            <p>From Media Campaigns, to Digital Application builds, to Lead Generation Initiatives and Brand Building Campaigns -
              the LACED Agency group is ready to assist you & your team, complimenting your current successes and
              collaborating on your next challenge. How can we help your team? Give us a buzz today!</p>
            <div class="direction-wrapper"><a href="https://goo.gl/maps/sWNg7iCADoC2" target="_blank" class="more-details">GET DIRECTIONS</a></div>
          </div>
          <div class="contact-form-container">
            <div class="form-result" style="display: none;"></div>
            <form id="contact-form">
              <input type="text" name="name" placeholder="Name" class="input-form">
              <input type="text" name="company" placeholder="Company" class="input-form">
              <input type="text" name="email" placeholder="Email Address" class="input-form">
              <input type="text" name="phone" placeholder="Phone Number" class="input-form">
              <textarea name="tell_us" placeholder="Tell us a little more..." class="textarea-form"></textarea>
              <input type="hidden" name="form_type" value="FC">
              <input type="submit" value="Submit" class="submit-btn">
            </form>
          </div>
        </div>
      </section>


      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
    <script src="js/contact_form.js"></script>
  </body>
</html>