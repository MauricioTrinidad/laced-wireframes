<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section default">
      <section>
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/Desktop_1440x810_1x_100w_HeroImageGFX.jpg, images/Desktop_2880x1620_2x_100w_HeroImageGFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/iPadLandscape_1024x576_1x_100w_HeroImageGFX.jpg, images/iPadLandscape_2048x1152_2x_100w_HeroImageGFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/iPadPortrait_768x432_1x_100w_HeroImageGFX.jpg, images/iPadPortrait_1536x864_2x_100w_HeroImageGFX.jpg 2x">
            <source media="" srcset="images/Mobile_414x736_1x_100w_HeroImageGFX.jpg, images/Mobile_828x1472_2x_100w_HeroImageGFX.jpg 2x">
            <img>
          </picture>
        </div>
      </section>
      <section class="work-container">
        <div class="title-section"><h1>WORK</h1></div>
        <div class="work-content">
          <div class="work-items clearfix">

            <div class="item">
              <a href="/work-ubm-le-awards"><picture>
                  <source media="(min-width: 769px)" srcset="images/480x270-1x-Desktop-TabletLG-Work-UBM-gfx.jpg, images/720x405-2x-Desktop-TabletLG-Work-UBM-gfx.jpg 2x">
                  <source media="" srcset="images/384x216-1x-TabletSm-Mobile-Work-UBM-gfx.jpg, images/576x324-2x-TabletSm-Mobile-Work-UBM-gfx.jpg 2x">
                  <img class="img-responsive">
                </picture></a>
              <div class="item-wrapper no-padding-tablet-desktop">
                <h2>
                  UBM: Social Media Audience Impact, Brand Building, &amp; Event Registrations.
                </h2>
                <a href="/work-ubm-le-awards" class="more-details">MORE DETAILS</a>
              </div>
            </div>

            <div class="item">
              <a href="/work-dremeldepot"><picture>
                  <source media="(min-width: 769px)" srcset="images/480x270-1x-Desktop-TabletLG-Work-DremelDepot-gfx.jpg, images/720x405-2x-Desktop-TabletLG-Work-DremelDepot-gfx.jpg 2x">
                  <source media="" srcset="images/384x216-1x-TabletSm-Mobile-Work-DremelDepot-gfx.jpg, images/576x324-2x-TabletSm-Mobile-Work-DremelDepot-gfx.jpg 2x">
                  <img class="img-responsive">
                </picture></a>
              <div class="item-wrapper no-padding-tablet-desktop">
                <h2>
                  DremelDepot: Facebook-esque Application connecting DIYers.
                </h2>
                <a href="/work-dremeldepot" class="more-details">MORE DETAILS</a>
              </div>
            </div>

            <div class="item">
              <a href="/work-jamba-juice"><picture>
                  <source media="(min-width: 769px)" srcset="images/480x270-1x-Desktop-TabletLG-Work-jamba-juice-gfx.jpg, images/720x405-2x-Desktop-TabletLG-Work-jamba-juice-gfx.jpg 2x">
                  <source media="" srcset="images/384x216-1x-TabletSm-Mobile-Work-jamba-juice-gfx.jpg, images/576x324-2x-TabletSm-Mobile-Work-jamba-juice-gfx.jpg 2x">
                  <img class="img-responsive">
                </picture></a>
              <div class="item-wrapper no-padding-tablet-desktop no-padding-bottom-tablet">
                <h2>
                  JambaJuice: Serving up a series of media campaigns
                </h2>
                <a href="/work-jamba-juice" class="more-details">MORE DETAILS</a>
              </div>
            </div>
            <div class="item">
              <a href="/work-victory"><picture>
                  <source media="(min-width: 769px)" 
                          srcset="images/480x270-1x-Desktop-TabletLG-Work-Victory-gfx.jpg,
                          images/720x405-2x-Desktop-TabletLG-Work-Victory-gfx.jpg 2x">
                  <source media="" 
                          srcset="images/384x216-1x-TabletSm-Mobile-Work-Victory-gfx.jpg,
                          images/576x324-2x-TabletSm-Mobile-Work-Victory-gfx.jpg 2x">
                  <img class="img-responsive">
                </picture></a>
              <div class="item-wrapper no-padding-tablet-desktop no-padding-bottom-mobile">
                <h2>
                  Victory Athletic: Harnessing the Power of Data to Drive e-commerce traffic & sales.
                </h2>
                <a href="/work-victory" class="more-details">MORE DETAILS</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section>
        <div class="title-section"><h1>SERVICES</h1></div>
        <div class="clearfix">
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-strategy.png">
            <div class="heading"><h2>LACED Strategy</h2></div>
            <p class="caption">The Game Plan: 17+ years experience, combined with a collaboration of creatives, fresh thinkers, and the curious - shaping ideas that make a difference.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-media.png">
            <div class="heading"><h2>LACED Media</h2></div>
            <p class="caption">Amplify the View: How we help you achieve multi-channel success, capturing the fast moving consumer at scale & on budget.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-marketing.png">
            <div class="heading"><h2>LACED Marketing</h2></div>
            <p class="caption">Pulling your audience in: Delivering Brand resonance that truly matters, one that gains customer investment & long-term loyalty.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-social.png">
            <div class="heading"><h2>LACED Social</h2></div>
            <p class="caption">The way your brand should think and act: influencing your audience and being influenced by them. Social Media can drive your audience to act, buy, and join your brand's tribe.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-production.png">
            <div class="heading"><h2>LACED Production</h2></div>
            <p class="caption">You bring the vision - our talented team of creatives, designers, programmers, and technical engineers make it a reality.</p>
            <a class="more-details">Learn More</a>
          </div>
          <div class="item-wrapper col-sm-2 col-md-3 border info-box">
            <img class="icon" src="images/icon-video.png">
            <div class="heading"><h2>LACED Video</h2></div>
            <p class="caption">Award Winning Visuals: Storytelling that resonates - from digital spots, corporate videos, how to’s, edutainment, live video events, & media spots.</p>
            <a class="more-details">Learn More</a>
          </div>
        </div>
      </section>
      
      <section class="blog-container">
        <div class="title-section"><h1>THINKING</h1></div>
        <div class="blog-content">
          <div class="item">
            <picture>
              <source media="(min-width: 1280px)" 
                      srcset="images/thinking/SEO-LACED-thinking.jpg">
              <source media="(min-width: 769px)" 
                      srcset="images/Desktop-Tablet-Landscape-720x405_1x-Careers-Image-GFX.jpg">
              <source media="(min-width: 431px)" 
                      srcset="images/Tablet-Portrait-384x216_1x_Careers-Image-GFX.jpg,
                      images/Tablet-Portrait-576x324_2x_Careers-Image-GFX.jpg 2x">
              <source media="" 
                      srcset="images/Mobile-192x108_1x_Careers-Image-GFX.jpg,
                      images/Mobile-288x162_2x_Careers-Image-GFX.jpg 2x">
              <img class="img-responsive">
            </picture>
            <div class="item-wrapper no-padding-bottom">
              <h2>
                SEO in 2017: The Rules Have Changed - What You Need to Know
              </h2>
              <a href="http://blog.lacedagency.com" target="_blank" class="more-details">VIEW BLOG POST</a>
            </div>
          </div>
        </div>
      </section>
      
      <section class="awards-container">
        <div class="title-section"><h1>AWARDS</h1></div>
        <div class="awards-content">
          <div class="awards-items clearfix">
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/davey-awards-gold-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Marketing Effectiveness<br>Best Integrated Campaign</span>
              </p>
            </div>
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/davey-awards-silver-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Marketing Effectiveness<br>Online Marketing</span>
              </p>
            </div>
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/w3-award-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Integrated Campaign<br>&nbsp;</span>
              </p>
            </div>
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/w3-award-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Digital Video Series<br>&nbsp;</span>
              </p>
            </div>
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/w3-award-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Media & Banner Campaign<br>&nbsp;</span>
              </p>
            </div>
            <div class="item">
              <picture>
                <source media="" 
                        srcset="images/w3-award-statue.png">
                <img class="img-responsive">
              </picture>
              <p>
                <span class="award-year">2016</span><br>
                <span class="award-copy">Marketing Effectiveness<br>&nbsp;</span>
              </p>
            </div>
          </div>
        </div>
      </section>
      
      <section class="clients-container">
        <div class="title-section"><h1>CLIENTS</h1></div>
        <div class="clients-content">
          <div class="clients-items clearfix">

            <figure class="item">
              <picture>
                <source media="" 
                        srcset="images/200x160-HomeDepot-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item">
              <picture>
                <source media="" 
                        srcset="images/260x160-UTI-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item border-right-light border-right-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/300x160-BoschLogo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top border-top-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/210x160-UBM-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top border-top-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/210x160-Victory-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top border-top-tablet-desktop border-right-light">
              <picture>
                <source media="" 
                        srcset="images/302x160-Williams-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top">
              <picture>
                <source media="" 
                        srcset="images/300x160-Dremel-Logo.png">
                <img>
              </picture>
            </figure> 
            <figure class="item no-border-top border-right-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/290x160-Navman-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item border-right-light no-border-top border-right-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/310x160-United-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top">
              <picture>
                <source media="" 
                        srcset="images/222x160-Hilton-Logo.png">
                <img>
              </picture>
            </figure>
            <figure class="item no-border-top">
              <picture>
                <source media="" 
                        srcset="images/280x160-Skill-Logo.png">
                <img>
              </picture>
            </figure> 
            <figure class="item border-right-light no-border-top border-right-tablet-desktop">
              <picture>
                <source media="" 
                        srcset="images/320x160-JambaJuice-Logo.png">
                <img>
              </picture>
            </figure> 
          </div>
        </div>
      </section>
      
      <section class="careers-container">
        <div class="title-section"><h1>CAREERS</h1></div>
        <div class="careers-content">
          <div class="one-half clearfix">
            <div class="item">
              <picture>
                <source media="(min-width: 1280px)" 
                        srcset="images/Desktop-960x540_2x-Careers-Image-GFX.jpg">
                <source media="(min-width: 769px)" 
                        srcset="images/Desktop-Tablet-Landscape-720x405_1x-Careers-Image-GFX.jpg">
                <source media="(min-width: 431px)" 
                        srcset="images/Tablet-Portrait-384x216_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Portrait-576x324_2x_Careers-Image-GFX.jpg 2x">
                <source media="" 
                        srcset="images/Mobile-192x108_1x_Careers-Image-GFX.jpg,
                        images/Mobile-288x162_2x_Careers-Image-GFX.jpg 2x">
                <img class="img-responsive">
              </picture>
            </div>
            <div class="item">
              <picture>
                <source media="(min-width: 1280px)" 
                        srcset="images/Desktop-960x540_2x-Careers-Image-GFX.jpg">
                <source media="(min-width: 769px)" 
                        srcset="images/Desktop-Tablet-Landscape-720x405_1x-Careers-Image-GFX.jpg">
                <source media="(min-width: 431px)" 
                        srcset="images/Tablet-Portrait-384x216_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Portrait-576x324_2x_Careers-Image-GFX.jpg 2x">
                <source media="" 
                        srcset="images/Mobile-192x108_1x_Careers-Image-GFX.jpg,
                        images/Mobile-288x162_2x_Careers-Image-GFX.jpg 2x">
                <img class="img-responsive">
              </picture>
            </div>
          </div>
          <div class="one-third clearfix">
            <div class="item">
              <picture>
                <source media="(min-width: 769px)" 
                        srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
                <source media="(min-width: 431px)" 
                        srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
                <source media="" 
                        srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                        images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
                <img class="img-responsive">
              </picture>
            </div>
            <div class="item">
              <picture>
                <source media="(min-width: 769px)" 
                        srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
                <source media="(min-width: 431px)" 
                        srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
                <source media="" 
                        srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                        images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
                <img class="img-responsive">
              </picture>
            </div>
            <div class="item">
              <picture>
                <source media="(min-width: 769px)" 
                        srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
                <source media="(min-width: 431px)" 
                        srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                        images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
                <source media="" 
                        srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                        images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
                <img class="img-responsive">
              </picture>
            </div>
          </div>
        </div>
      </section>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>