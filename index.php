<?php

$params = explode('/', filter_input(INPUT_SERVER, 'REQUEST_URI'));
$routes = ['default', 'work', 'work-dremeldepot', 'work-rexroth','work-victory', 'work-navman', 'work-williams', 'work-ubm-le-awards', 'work-ubm-le-2015', 'services-driver-recruitment', 'contact', 'services', 'careers', 'work-jamba-juice'];
$slug = $params['1'];

//var_dump($params);
//var_dump(filter_input(INPUT_SERVER, 'REQUEST_URI'));

if ($slug == '') {
    include('default.php');
} else {
    if (in_array($slug, $routes)) {
        include($slug.'.php');
    } else {
        include('404.html');
    }
}
