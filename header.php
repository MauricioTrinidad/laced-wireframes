
<header>
    <div class="clearfix top-header">
        <a href="/" class="logo-laced"><img src="images/LogoWhite2.png" width="200" height="44"></a>
        <div id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="nav-wrapper">
        <div>
            <nav>
                <a <?php if ($slug == ''): ?>class="current"<?php endif; ?> href="/">HOME</a>
                <a <?php if (strpos($slug, 'work') !== false): ?>class="current"<?php endif; ?> href="/work">WORK</a>
                <a <?php if ($slug == 'services'): ?>class="current"<?php endif; ?> href="/services">SERVICES</a>
                <a href="http://blog.lacedagency.com">THINKING</a>
                <a <?php if ($slug == 'careers'): ?>class="current"<?php endif; ?> href="/careers">CAREERS</a>
                <a <?php if ($slug == 'contact'): ?>class="current"<?php endif; ?> href="/contact">CONTACT</a>
            </nav>
            <ul class="menu-social-links clearfix">
                <li><a href="https://www.facebook.com/LACEDagency/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/lacedtweet" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.linkedin.com/company/laced-interactive" target="_blank"><i class="fa fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="https://www.youtube.com/user/lacedinteractive" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
            <address class="menu">
                <div class="menu-phone">
                    <p>Call 310-316-1962</p>
                </div>
                <div class="menu-address">
                    <p>
                        Google Maps<br>
                        114 S Catalina Avenue, Redondo Beach, CA 90277
                    </p>
                </div>
            </address>
        </div>
    </div>
    <div class="form-wrapper">
        <div>
            <p>Tell Us About Your Project.</p>
            <div class="form-result-quick" style="display: none;"></div>
            <form id="project-form">
                <input type="text" name="name" placeholder="Your Name" class="input-form">
                <input type="text" name="email" placeholder="Email Address" class="input-form">
                <input type="text" name="phone" placeholder="Phone Number" class="input-form">
                <textarea name="project_brief" rows="8" placeholder="Project Brief" class="textarea-form"></textarea>
                <input type="hidden" name="form_type" value="FP">
                <input type="submit" value="Submit" class="submit-btn">
            </form>
        </div>
    </div>
</header>