<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" 
                    srcset="images/work-jamba/Desktop-1980x1080-1x-JambaJuice-Hero-GFX.jpg,
                    images/work-jamba/Desktop-2880x1620--2x-JambaJuice-Hero-GFX.jpg 2x">
            <source media="(min-width: 769px)" 
                    srcset="images/work-jamba/Tablet-lg-1280x720-1x-JambaJuice-Hero-GFX.jpg,
                    images/work-jamba/Tablet-lg-1920x1080-2x-JambaJuice-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" 
                    srcset="images/work-jamba/Tablet-sm-768x432-1x-JambaJuice-Hero-GFX.jpg,
                    images/work-jamba/Tablet-sm-1152x648-2x-JambaJuice-Hero-GFX.jpg 2x">
            <source media="" 
                    srcset="images/work-jamba/Mobile-414x55-1x-JambaJuice-Hero-GFX.jpg,
                    images/work-jamba/Mobile-621x828-2x-JambaJuice-Hero-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>The Challenge</h2>
          </div>
          <p>Navman Wireless was in the midst of a new product launch with the Qtanium 300 – an IP-67 rated, heavy duty tracking device. as they were rolling out press releases, and product brochures they discovered that their audience was a little bit confused on what the device actually did or challenge it solved. Navman Wireless needed a video solution. They needed a product video that clearly communicated what the new product (the Qtanium 300 – an IP-67 rated, heavy duty tracking device) and it’s features, what challenge it solved, and how it did it. More importantly, this video needed to be entertaining and easy to understand. Navman Wireless also requested a longer infomercial-type product video, as they wanted to play the finished product at tradeshows on repeat in the sales booth to attract bigger sales. LACED Agency was up for the challenge, as well as the short turnaround time.</p>
          <p>NAVMAN Wireless Holdings is a leading global provider of real-time fleet tracking solutions as well as GPS-based tracking solutions for the OEM market.</p>
          <p>Founded in New Zealand in 1988, Navman Wireless introduced its first fleet-tracking products in 2001 and was acquired by Brunswick Corporation in 2003. Brunswick sold the company in 2007 to an investment group that has built the independent company into a market leader in both the fleet tracking and OEM segments.</p>
          <p>Navman Wireless fleet tracking customers include trucking and delivery companies, limousine services, building contractors and other service providers.</p>
        </div>
      </section>

        <section class="detail-section">
            <picture class="content">
                <source media="(min-width: 1280px)"
                        srcset="images/Desktop-1600x900-1x-JambaJuice-iphone-GFX.jpg,
                images/work-jamba/Desktop-2160x1215-2x-JambaJuice-iphone-GFX.jpg 2x">
                <source media="(min-width: 769px)"
                        srcset="images/work-jamba/Tablet-lg-1280x720-1x-JambaJuice-iphone-GFX.jpg,
                images/work-jamba/Tablet-lg-1920x1080-2x-JambaJuice-iphone-GFX.jpg 2x">
                <source media="(min-width: 431px)"
                        srcset="images/work-jamba/Tablet-sm-768x432-1x-JambaJuice-iphone-GFX.jpg,
                images/work-jamba/Tablet-sm-1152x648-2x-JambaJuice-iphone-GFX.jpg 2x">
                <source media=""
                        srcset="images/work-jamba/Mobile-414x233-1x-JambaJuice-iphone-GFX.jpg,
                images/work-jamba/Mobile-621x349-2x-JambaJuice-iphone-GFX.jpg 2x">
                <img class="img-responsive">
            </picture>
            <div class="detail-content-wrapper">
                <div>
                    <h2>Making It Responsive</h2>
                </div>
                <p>DIYers are on the go; in the yard, on the job site, or the shed – and that means mobile! LACED Agency designed the DremelDepot.com community to be responsive; meaning accessible on the desktop, mobile, and tablet devices. So when ever or where ever you are – Dremel Depot stays with you. The mobile site is as rich in functionality and features as the desktop version and provides the same interactive DIY Community experience. Users can easily like, comment, and see the number of views on any project, navigate with an easy menu bar, search for Projects, view the Project Gallery, upload a project of their own, view their messages, and visit their accounts….all in the palm of your hand. DremelDepot.com Connecting the experienced and emerging DIYers together in one community – accessible on all your devices, all the time.</p>
            </div>
        </section>

        <section class="jamba-juice-banners detail-section">
            <div class="banner-to-hide">
                <iframe frameborder="0" src="/html5-anim/728x90-FruitandVeg.html" width="728" height="90"></iframe>
            </div>
            <div class="clearfix">
                <div class="col-banner-1 banner-to-hide">
                    <iframe frameborder="0" src="/html5-anim/160x600_curves_hi_4.html" width="160" height="600"></iframe>
                </div>
                <div class="col-banner-2">
                    <iframe frameborder="0" src="/html5-anim/300x250-FruitandVeg.html" width="300" height="250"></iframe>
                </div>
            </div>
        </section>

        <section class="detail-section">
            <div class="detail-content-wrapper no-padding-top padding-80-top">
                <div class="no-margin-bottom">
                    <h2>Ongoing Marketing: Community Engagement</h2>
                    <p>Communication is paramount to any successful relationship – offline or online, business or personal. Building a digital community demands sophisticated communication functionality and LACED delivered. The MESSAGES box alerts users to notifications; when users “like” or “comment” on a project of theirs, as well as Notifications from Dremel regarding new promotions or earning reward badges. Users also receive personal emails when they engage that community; joining receives a “welcome email”, various community behaviors receive “Congrats! You have earned___new badge!”, etc… This not only helps keep users engagement with DremelDepot but also drove excitement for users.</p>
                </div>
        </section>

        <section class="detail-split-section clearfix">
            <div class="left">
                <picture class="content">
                    <source media="(min-width: 431px)"
                            srcset="images/work-jamba/Desktop-960x540-1x-JambaJuice-iphone-GFX.jpg,
                            images/work-jamba/Desktop-1152x648-2x-JambaJuice-FlowChart-GFX.jpg 2x">
                    <source media=""
                            srcset="imageswork-jamba/Mobile-414x233-1x-JambaJuice-FlowChart-GFX.jpg,
                    images/work-jamba/Mobile-621x349-2x-JambaJuice-FlowChart-GFX.jpg 2x">
                    <img class="img-responsive">
                </picture>
            </div>
            <div class="right detail-content-wrapper">
                <div>
                    <h2>Dremeldepot’s Pre-launch Marketing efforts included a landing page, quick sign-up form for those users who were interested in getting an early invite to the community or being the first to try it out once the formal launch rolled out.</h2></div>
            </div>
        </section>

        <section class="detail-section">
            <picture class="content">
                <source media="(min-width: 1280px)"
                        srcset="images/Desktop-1600x900-1x-JambaJuice-Pumpkin-Smash-GFX.jpg,
                images/work-jamba/Desktop-2160x1215-2x-JambaJuice-Pumpkin-Smash-GFX.jpg 2x">
                <source media="(min-width: 769px)"
                        srcset="images/work-jamba/Tablet-lg-1280x720-1x-JambaJuice-Pumpkin-Smash-GFX.jpg,
                images/work-jamba/Tablet-lg-1920x1080-2x-JambaJuice-Pumpkin-Smash-GFX.jpg 2x">
                <source media="(min-width: 431px)"
                        srcset="images/work-jamba/Tablet-sm-768x432-1x-JambaJuice-Pumpkin-Smash-GFX.jpg,
                images/work-jamba/Tablet-sm-1152x648-2x-JambaJuice-Pumpkin-Smash-GFX.jpg 2x">
                <source media=""
                        srcset="images/work-jamba/Mobile-414x233-1x-JambaJuice-Pumpkin-Smash-GFX.jpg,
                images/work-jamba/Mobile-621x349-2x-JambaJuice-Pumpkin-Smash-GFX.jpg 2x">
                <img class="img-responsive">
            </picture>
            <div class="detail-content-wrapper">
                <div>
                    <h2>Making It Responsive</h2>
                </div>
                <p>DIYers are on the go; in the yard, on the job site, or the shed – and that means mobile! LACED Agency designed the DremelDepot.com community to be responsive; meaning accessible on the desktop, mobile, and tablet devices. So when ever or where ever you are – Dremel Depot stays with you. The mobile site is as rich in functionality and features as the desktop version and provides the same interactive DIY Community experience. Users can easily like, comment, and see the number of views on any project, navigate with an easy menu bar, search for Projects, view the Project Gallery, upload a project of their own, view their messages, and visit their accounts….all in the palm of your hand. DremelDepot.com Connecting the experienced and emerging DIYers together in one community – accessible on all your devices, all the time.</p>
            </div>
        </section>


      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>