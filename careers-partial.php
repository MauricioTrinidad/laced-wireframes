<section class="careers-container">
  <div class="careers-content">
    <div class="one-half clearfix">
      <div class="item">
        <picture>
          <source media="(min-width: 1280px)" 
                  srcset="images/Desktop-960x540_2x-Careers-Image-GFX.jpg">
          <source media="(min-width: 769px)" 
                  srcset="images/Desktop-Tablet-Landscape-720x405_1x-Careers-Image-GFX.jpg">
          <source media="(min-width: 431px)" 
                  srcset="images/Tablet-Portrait-384x216_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Portrait-576x324_2x_Careers-Image-GFX.jpg 2x">
          <source media="" 
                  srcset="images/Mobile-192x108_1x_Careers-Image-GFX.jpg,
                  images/Mobile-288x162_2x_Careers-Image-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </div>
      <div class="item">
        <picture>
          <source media="(min-width: 1280px)" 
                  srcset="images/Desktop-960x540_2x-Careers-Image-GFX.jpg">
          <source media="(min-width: 769px)" 
                  srcset="images/Desktop-Tablet-Landscape-720x405_1x-Careers-Image-GFX.jpg">
          <source media="(min-width: 431px)" 
                  srcset="images/Tablet-Portrait-384x216_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Portrait-576x324_2x_Careers-Image-GFX.jpg 2x">
          <source media="" 
                  srcset="images/Mobile-192x108_1x_Careers-Image-GFX.jpg,
                  images/Mobile-288x162_2x_Careers-Image-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </div>
    </div>
    <div class="one-third clearfix">
      <div class="item">
        <picture>
          <source media="(min-width: 769px)" 
                  srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
          <source media="(min-width: 431px)" 
                  srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
          <source media="" 
                  srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                  images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </div>
      <div class="item">
        <picture>
          <source media="(min-width: 769px)" 
                  srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
          <source media="(min-width: 431px)" 
                  srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
          <source media="" 
                  srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                  images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </div>
      <div class="item">
        <picture>
          <source media="(min-width: 769px)" 
                  srcset="images/Tablet-Landscape-Desktop-430x430_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Landscape-Desktop-649x649_2x_Careers-Image-GFX.jpg 2x">
          <source media="(min-width: 431px)" 
                  srcset="images/Tablet-Portrait-256x256_1x_Careers-Image-GFX.jpg,
                  images/Tablet-Portrait-384x384_2x_Careers-Image-GFX.jpg 2x">
          <source media="" 
                  srcset="images/Mobile-125x125_1x_Careers-Image-GFX.jpg,
                  images/Mobile-187x187_2x_Careers-Image-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </div>
    </div>
  </div>
</section>