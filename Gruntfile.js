module.exports = function (grunt) {

    grunt.initConfig({
        copy: {
            staging: {
                files: [
                    {
                        expand: true,
                        src: [
                            'css/**',
                            'images/**',
                            'js/**',
                            '*.php'
                        ],
                        dest: 'staging/'
                    }
                ]
            }
        },
        clean: {
            staging: 'staging/**'
        },
        cssmin: {
            staging: {
                options: {
                    report: 'gzip'
                },
                files: [{
                    expand: true,
                    cwd: 'staging/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'staging/css/',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            staging: {
                files: {
                    'staging/js/main.min.js': ['js/main.js', 'js/video.js']
                }
            }
        },
        'string-replace': {
            staging: {
                files: {
                    'staging/': [
                        'head.php', 'scripts.php'
                    ]
                },
                options: {
                    replacements: [
                        {
                            pattern: /\.css/gi,
                            replacement: '.min.css'
                        },
                        {
                            pattern: /\main.js/gi,
                            replacement: 'main.min.js'
                        },
                        {
                            pattern: '<script src="/js/video.js"></script>',
                            replacement: ''
                        }
                    ]
                }
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-string-replace');
    
    grunt.registerTask('default', ['clean', 'copy', 'string-replace', 'cssmin', 'uglify']);
};