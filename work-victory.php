<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)"
                    srcset="images/Desktop-1920x1080_1x_Victory-HeroBanner-GFX.jpg,
                    images/Desktop-2880x1620_2x_Victory-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)"
                    srcset="images/Tablet-lg-1280x720-1x_Victory-HeroBanner-GFX.jpg,
                    images/Tablet-lg-1920x1080-2x-Victory-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)"
                    srcset="images/Tablet-sm-768x432-1x-Victory-HeroBanner-GFX.jpg,
                    images/Tablet-sm-1152x648-2x-Victory-HeroBanner-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x552_1x_Victory-HeroBanner-GFX.jpg,
                    images/Mobile-621x828-2x-Victory-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>The "Glove Love" Campaign</h2>
          </div>
          <p>Named “The Glove Love Campaign” LACED Agency was focused on increasing Victory brand awareness and driving traffic to Victory’s e-commerce site during a focused 1 week promotion during January, a traditonally challenging month for sales after the holidays. The Campaign included; 1) One Email Blast to (2) Two Segmented Email Lists, and an Instagram Contest.</p>
          <h3>Getting Started</h3>
          <p>LACED continued efforts to clean Victory’s customer data before launching the Email Campaigns. This included LACED Agency removing all unsubscribes, bounces, and spam emails – as well as cross-checking against REMOVE Lists. Additionally, we added new “opt-in” emails from newsletter website signups, and in-store visits from the successful holiday run this past November and December. All lists were cleaned and then programmatically double-checked for optimal quality assurance through software. Finalizing our two lists, we named them List A and List B. List A was composed of Victory’s most recent opt-ins, and active customers from 2012, 2013, 2014. List B was a combined list of all past customers as well as inactive customers dating back as far as 1990.</p>
          <h3>Quality Assurance & Ongoing Testing</h3>
          <p>LACED Agency planned on continuing to do A & B Testing on both lists separately in order to identify customers whom were true enthusiasts, those that were active customers, those that were passive customers, and those that were inactive. Additionally, LACED Agency segmented customers by building behavioral buckets of data with each campaign, to better personalize future email marketing & automation, marketing to each customer’s specific needs, interests, clicks and purchases to add more value and a better experience with Victory’s brand.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540_1x_Victory-FlowChart-Image1-GFX.jpg,
                    images/414-px-1152x648-2x-Victory-FlowChart-Image1-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x-Victory-FlowChart-Image1-GFX.jpg,
                    images/Mobile-621x349-2x-Victory-FlowChart-Image1-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>LACED Agency segmented customers by building behavioral buckets of data with each campaign, to better personalize future email marketing</h2>
          </div>
        </div>
      </section>
      
      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
              <h2>Creative</h2>
          <p>Using Victory batting gloves as our main creative element, LACED came up with the idea for GLOVE “LOVE”. The concept was to take Victory batting gloves and make them the visual focus for the overall campaign, pointing out the specs that make them competitive in the market. The campaign consisted of timely eMail Marketing, as well as a Instagram Social Media contest.</p>
        </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/Desktop-1600x900-1x-Victory-Creative-Image2-GFX.jpg,
                  images/Desktop-2160x215-2x-Victory-Creative-Image2-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/Tablet-lg-1280x720-1x-Victory-Creative-Image2-GFX.jpg,
                  images/Tablet-lg-1920x1080-2x-Victory-Creative-Image2-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/Tablet-sm-768x432-1x-Victory-Creative-Image2-GFX.jpg,
                  images/Tablet-sm-1152x648-2x-Victory-Creative-Image2-GFX.jpg 2x">
          <source media=""
                  srcset="images/Mobile-414x233-1x-Victory-Creative-Image2-GFX.jpg,
                  images/Mobile-621x349-2x-Victory-Creative-Image2-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Social Media Contest</h2>
          </div>
          <p>Leveraging <a href="http://www.instagram.com/victorygear" target="_blank">@victorygear</a> on Instagram, LACED ran a Social Media Contest, where Instagram followers had a chance to win a pair of Victory batting gloves & a #FEELVICTORY T-shirt. Sticking with the GLOVE "LOVE" campaign theme, LACED designed each letter of the word GLOVE with Victory batting gloves. Victory Instagram followers had a chance to win by reposting each letter to reveal a sercret word over a 5 day period.</p>
        </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/Desktop-1600x900-1x-Victory-InstagramContest-Image3-GFX.jpg,
                  images/Desktop-2160x215-2x-Victory-InstagramContest-Image3-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/Tablet-lg-1280x720-1x-Victory-InstagramContest-Image3-GFX.jpg,
                  images/Tablet-lg-1920x1080-2x-Victory-InstagramContest-Image3-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/Tablet-sm-768x432-1x-Victory-InstagramContest-Image3-GFX.jpg,
                  images/Tablet-sm-1152x648-2x-Victory-InstagramContest-Image3-GFX.jpg 2x">
          <source media=""
                  srcset="images/Mobile-414x233-1x-Victory-InstagramContest-Image3-GFX.jpg,
                  images/Mobile-621x349-2x-Victory-InstagramContest-Image3-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Results</h2>
          </div>
          <p>As a result of the "Glove Love" Campaign:
          <ul>
            <li>41% Emails Read</li>
            <li>9% Emails Clicked</li>
            <li>24% Instagram Followers</li>
            <li>34% Batting Glove Sales</li>
          </ul>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540-1x_Victory-ContestWinner-Image4-GFX.jpg,
                    images/414-px-1152x648_2x_Victory-ContestWinner-Image4-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x_Victory-ContestWinner-Image4-GFX.jpg,
                    images/Mobile-621x349-2x_Victory-ContestWinner-Image4-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>The "Glove Love" Instagram contest, dramatically increased the number of Victory Custom Athletics's followers, and promoted a high level of user engagement.</h2>
        </div>
      </section>


      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>