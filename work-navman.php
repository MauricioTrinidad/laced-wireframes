<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" 
                    srcset="images/Desktop-1920x1080_1x-Navman-HeroBanner-GFX.jpg,
                    images/Desktop-2880x1620-2x-Navman-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)" 
                    srcset="images/Tablet-lg-1280x720-1x-Navman-HeroBanner-GFX.jpg,
                    images/Tablet-lg-1920x1080-2x-Navman-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)" 
                    srcset="images/Tablet-sm-768x432-1x-Navman-HeroBanner-GFX.jpg,
                    images/Tablet-sm-1152x648-2x-Navman-HeroBanner-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x552-1x-Navman-HeroBanner-GFX.jpg,
                    images/Mobile-621x828-2x-Navman-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>The Challenge</h2>
          </div>
          <p>Navman Wireless was in the midst of a new product launch with the Qtanium 300 – an IP-67 rated, heavy duty tracking device. as they were rolling out press releases, and product brochures they discovered that their audience was a little bit confused on what the device actually did or challenge it solved. Navman Wireless needed a video solution. They needed a product video that clearly communicated what the new product (the Qtanium 300 – an IP-67 rated, heavy duty tracking device) and it’s features, what challenge it solved, and how it did it. More importantly, this video needed to be entertaining and easy to understand. Navman Wireless also requested a longer infomercial-type product video, as they wanted to play the finished product at tradeshows on repeat in the sales booth to attract bigger sales. LACED Agency was up for the challenge, as well as the short turnaround time.</p>
          <p>NAVMAN Wireless Holdings is a leading global provider of real-time fleet tracking solutions as well as GPS-based tracking solutions for the OEM market.</p>
          <p>Founded in New Zealand in 1988, Navman Wireless introduced its first fleet-tracking products in 2001 and was acquired by Brunswick Corporation in 2003. Brunswick sold the company in 2007 to an investment group that has built the independent company into a market leader in both the fleet tracking and OEM segments.</p>
          <p>Navman Wireless fleet tracking customers include trucking and delivery companies, limousine services, building contractors and other service providers.</p>
        </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/navman/1920x1080-1x-Navman-video-gfx.jpg">
            <source media="" srcset="images/navman/621x349-2x-Navman-video-gfx.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="zLaNA4Y-yFw">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>The Solution</h2>
          </div>
          <p>Collaborating with the Navman Wireless marketing team, LACED Agency successfully created and deployed the digital video for the product launch of NAVMAN Wireless’s newsest product, Qtanium 300.</p>
          <p>NAVMAN Wireless has long been a world leader in GPS-Based Technology and Information for vehicle tracking. Their fleet tracking solutions help these customers derive significant cost savings from reduced fuel usage, less vehicle wear and tear and lower maintenance costs. In addition, Navman Wireless systems provide significant dispatch and operating efficiencies, better communications with every vehicle in the fleet and improved compliance with government regulations.</p>
          <p>NAVMAN’s newest product, Qtanium 300 – an IP-67 rated, heavy duty tracking device developed for superior performance in demanding conditions – is taking center stage in a digital campaign roll-out this 2010 fall season. One of the key creative components of this campaign is a 7minute web infomercial or a <i>webfomercial</i> (as we call it in digital marketing).</p>
        </div>
      </section>


      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" 
                    srcset="images/414-px-960x540-1x-Navman-Quote-Image1-GFX.jpg,
                    images/414-px-1152x648-2x-Navman-Quote-Image1-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233-1x-Navman-Quote-Image1-GFX.jpg,
                    images/Mobile-621x349-1x-Navman-Quote-Image1-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>"LACED not only delivered an online webfomercial that clearly educates Navman’s target audience on the benefits of the Qtainium 300, but they also found a way to hold the attention of potential Navman buyers and audiences in an entertaining way. No simple feat to accomplish with online video. Both Navman and the Group365 team couldn’t be happier with the final product. LACED Interactive did a fantastic job!"</h2>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>Telling The Story</h2>
            <p>Using 3-D type fonts, overlays, smart soundmix design, and sophisticated shooting angles – the NAVMAN Qtanium 300 video tells exactly what the product is, what it does, and how it benefits contractors as well as all professionals in construction around the world – online and accessible from any digital device worldwide.</p>
            <p> – as well as be a key partner in the overall digital marketing rollout, traffic generation, and analytics for the overall campaign.</p>
          </div>
      </section>


      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540-1x-Navman-StoryBoard-Image2-GFX.jpg,
                    images/414-px-1152x648-2x-Navman-StoryBoard-Image2-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x-Navman-StoryBoard-Image2-GFX.jpg,
                    images/Mobile-621x349-2x-Navman-StoryBoard-Image2-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>NAVMAN Wireless hired LACED Agency to handle the creative storyboarding, assist with script development, video shoot, sound mixing, video editing.</h2>
          </div>
        </div>
      </section>

      <section class="detail-section clearfix">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>Award Winning</h2>
            <p>Winner of the BMA Tower Awards for "Video Production - Sales, Product or Corporate Promotions".</p>
          </div>
      </section>

      <section class="detail-split-section clearfix" style="display: none;">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)" 
                    srcset="images/414-px-960x540_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/414-px-1152x648_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <source media="" 
                    srcset="images/Mobile-414x233_1x_DremelDepot-BadgeEmail-Image5-GFX.jpg,
                    images/Mobile-621x349_2x_DremelDepot-BadgeEmail-Image5-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>Split Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fermentum nisl dui, at tincidunt mi sagittis at.</h2>
            <!--<br class="work-detail-img-separator">-->
            <p>Fusce quis tellus efficitur, fringilla felis vel, lobortis mauris. Suspendisse maximus porta porta. Fusce sit amet lorem erat. Praesent eu magna scelerisque, varius leo eu, imperdiet nulla. Curabitur faucibus mi non leo semper, lacinia dictum sem varius. Sed dapibus tellus sem, non placerat augue aliquet quis. Proin elementum nibh sed velit elementum iaculis.</p>
          </div>
        </div>
      </section>


      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>