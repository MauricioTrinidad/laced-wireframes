
<?php

//header('Content-Type: application/json');
//echo json_encode(array('result' => 'SUCCESS', 'reason' => 'Message has been sent'));
//
//exit;

require 'vendor/phpmailer/PHPMailerAutoload.php';
if (!empty($_POST['form_type'])) {
    $form_type = $_POST['form_type'];

    $body = "";
    $subject = "";
    $altBody = "";
    $hasErrors = false;

    switch ($form_type) {
        case 'FP'://Form Project
            if (!empty($_POST['name']) &&
                                !empty($_POST['email']) &&
                                !empty($_POST['phone']) &&
                                !empty($_POST['project_brief'])) {

                $body = "<strong>Name:</strong> {$_POST['name']}<br>"
                                    . "<strong>Email:</strong> {$_POST['email']}<br>"
                                    . "<strong>Phone:</strong> {$_POST['phone']}<br>"
                                    . "<strong>Project Brief:</strong> {$_POST['project_brief']}";

                $subject = "LACED Project Contact Form";
                $altBody = "Project contact form";
            } else {
                $hasErrors = true;
            }

            break;
        case 'FC': //Form Contact
            if (!empty($_POST['name']) &&
                                !empty($_POST['email']) &&
                                !empty($_POST['phone']) &&
                                !empty($_POST['company']) &&
                                !empty($_POST['tell_us'])) {

                $body = "<strong>Name:</strong> {$_POST['name']}<br>"
                                    . "<strong>Email Address:</strong> {$_POST['email']}<br>"
                                    . "<strong>Phone Number:</strong> {$_POST['phone']}<br>"
                                    . "<strong>Company:</strong> {$_POST['company']}<br>"
                                    . "<strong>Tell us a little more:</strong> {$_POST['tell_us']}";

                $subject = "LACED Contact Form";
                $altBody = "Contact form";
            } else {
                $hasErrors = true;
            }
            break;
    }

    if (!$hasErrors) {
        $mail = new PHPMailer();

        $mail->setFrom('mwalsh@lacedagency.com', 'LACED Agency');
        $mail->addAddress('mwalsh@lacedagency.com', 'Michael Walsh');
        $mail->addReplyTo('no-reply@lacedagency.com', 'Information');
        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = $altBody;

        if (!$mail->send()) {
            header('Content-Type: application/json');
            echo json_encode(array('result' => 'SUCCESS', 'reason' => $mail->ErrorInfo));
        } else {
            header('Content-Type: application/json');
            echo json_encode(array('result' => 'SUCCESS', 'reason' => 'Message has been sent'));
        }
    }


    //
} else {
    header('Content-Type: application/json');
    echo json_encode(array('result' => 'ERROR', 'reason' => 'Invalid params'));
}



