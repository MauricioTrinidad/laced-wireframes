<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/Desktop-1920x1080_1x_UBM-LE-Awards-HeroBanner-GFX.jpg, images/Desktop-2880x1620-2x_UBM-LE-Awards-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720-1x_UBM-LE-Awards-HeroBanner-GFX.jpg, images/Tablet-lg-1920x1080-2x_UBM-LE-Awards-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432-1x_UBM-LE-Awards-HeroBanner-GFX.jpg, images/Tablet-sm-1152x648-2x_UBM-LE-Awards-HeroBanner-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x552-1x_UBM-LE-Awards-HeroBanner-GFX.jpg, images/Mobile-621x828-2x_UBM-LE-Awards-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>About Client</h2>
          </div>
          <p>UBM Advanstar, part of UBM Americas, is a US-based event and marketing services business serving the Fashion, Licensing, Automotive and Powersports industries. The company owns and operates a portfolio of 50 tradeshows, 5 publications, and over 30 electronic products and websites. Among their properties is The Licensing Expo, the largest international gathering of licensing brands, properties, agents, manufacturers, and distribution channels in the world. The show hosts over 90+ different countries and over 5000+ brands.</p>
        </div>
      </section>


      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/Desktop-1600x900-1x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg,
                  images/Desktop-2160x1215-2x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/Tablet-lg-1280x720-1x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg,
                  images/Tablet-lg-1920x1080-2x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/Tablet-sm-768x432-1x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg,
                  images/Tablet-sm-1152x648-2x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg 2x">
          <source media=""
                  srcset="images/Mobile-414x233-1x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg,
                  images/Mobile-621x349-2x-UBM-LE-Awards-People-Entering-Image1-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Agency Video Work</h2>
          </div>
          <p>LACED Agency was hired to encourage and drive overall show attendance, build greater brand awareness, and create excitement. A key component of the overall agency strategy for <i>The Licensing Expo Campaign</i> was to create a <i>Digital Video Commercial Series</i> designed to visually promote the show for both prospective and returning attendees.</p>
        </div>
      </section>

<!-- SECTION 50/50 with QUOTE -->
      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540-1x-UBM-LE-Awards-Hulk-Tyson-Image2-GFX.jpg,
                    images/414-px-1152x648-2x-UBM-LE-Awards-Hulk-Tyson-Image2-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x-UBM-LE-Awards-Hulk-Tyson-Image2-GFX.jpg,
                    images/Mobile-621x349-2x-UBM-LE-Awards-Hulk-Tyson-Image2-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper" style="background-color:#eaeaea;">
          <div>
            <h4><i>"The LACED Agency team produced eye-catching, engaging content for Licensing Expo leading up to the show and also providing on-site coverage for four full days. They were troopers from 7 AM to midnight almost every day, never missing an important event to cover at the show and treating our exhibitors and attendees with the utmost respect. Our social media engagement skyrocketed in the month leading up to and during the show, thanks to the great coverage and the unique content they produced. The Digital Video Commercial Series was key in the overall campaign's success - and was a HUGE hit among attendees."</i></h2>
            <h3>— Emilie Schwab</h3>
            <h4>Marketing Manager At UBM</h4>
          </div>
        </div>
      </section>


<!-- SECTION VIDEO 1 -->
	<section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080-1x-UBM-LE-Awards-1min-Video1-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x-UBM-LE-Awards-1min-Video1-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="kNJHO3OtDjA">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>2016 Licensing Expo - 1+ Minute Sizzle/Promo Video</h2>
          </div>
          <p>The 1st of three in the Video Series; this video was designed to be longer in length, providing the most in depth overview of the show (highlighting business categories, international countries attending, events, & overall excitement). The Video was rolled out to social media channels, advertising & media placements, email marketing, and web channels – 2 months prior to the show.</p>
        </div>
      </section>
      
<!-- SECTION VIDEO 2 -->
      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080-1x-UBM-LE-Awards-30sec-Video2-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621-349-2x-UBM-LE-Awards-30sec-Video2-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="wkTt6vh86fU">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>2016 Licensing Expo - :30 Second Sizzle/Promo Video</h2>
          </div>
          <p>The 2nd of three in the Video Series; this video was designed to be shorter in length, providing a :30sec Splash of the show (highlighting business categories, international countries attending, events, & overall excitement). The Video was rolled out to social media channels, advertising & media placements, email marketing, and web channels – 1 month prior to the show. It was also used throughout the live show and to open the main keynote.</p>
        </div>
      </section>


      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1920x1080-1x-UBM-LE-Awards-15sec-Video3-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x-UBM-LE-Awards-15sec-Video3-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="1" data-video="t2PRbLMCmZI">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>2016 Licensing Expo - :15 Second Sizzle/Promo Video</h2>
          </div>
          <p>The 3rd of three in the Video Series; this video was designed to be the shortest in length, providing a :15sec Teaser of the show to emphasis the most excitement (highlighting business categories, international countries attending, events, & overall excitement). The Video was rolled out to social media channels, advertising & media placements, email marketing, and web channels – 2 weeks before the official show start. It was also used throughout the live show.</p>
        </div>
      </section>

      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>