
<section>
  <div class="item-wrapper">
    <div>
      <h2>Trucking Recruitment: Delivering CDL Drivers that Stay for the Long-Haul</h2>
    </div>
  </div>
  <a href="/services-driver-recruitment"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-DriverRecruitment-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-DriverRecruitment-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-DriverRecruitment-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-DriverRecruitment-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>UBM: Social Media Driving Audience Impact, Brand Building, & Event Registrations.</h2>
    </div>
  </div>
  <a href="/work-ubm-le-awards"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-UBM-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-UBM-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-UBM-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-UBM-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>DremelDepot: Facebook-esque Application connecting the world’s most talented & novice DIYers.</h2>
    </div>
  </div>
  <a href="/work-dremeldepot"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-DremelDepot-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-DremelDepot-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-DremelDepot-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-DremelDepot-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>Victory Athletic: Harnessing the Power of Data to Drive e-commerce traffic & sales.</h2>
    </div>
  </div>
  <a href="/work-victory"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-Victory-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-Victory-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-Victory-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-Victory-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>Williams Products: Corporate Video highlights loyalty, quality HVAC/R product line, & brand legacy.</h2>
    </div>
  </div>
  <a href="/work-williams"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-Williams-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-Williams-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-Williams-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-Williams-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>Bosch Rexroth: Digital Product Catalog for a Global Sales Force, Sleek & Feature-Rich iPad Application.</h2>
    </div>
  </div>
  <a href="/work-rexroth"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-Rexroth-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-Rexroth-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-Rexroth-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-Rexroth-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>
<section>
  <div class="item-wrapper">
    <div>
      <h2>NAVMAN Wireless: Delivering a Digital Video Spot that sings new product sales. </h2>
    </div>
  </div>
  <a href="/work-navman"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-NavmanA-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-NavmanA-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-NavmanA-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-NavmanA-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>

<section>
  <div class="item-wrapper">
    <div>
      <h2>Jamba Juice: Media Campaigns & Data-Driven Direct Response, driving customers into stores nationwide.</h2>
    </div>
  </div>
  <a href="/work-jamba-juice"><picture class="content">
      <source media="(min-width: 769px)" 
              srcset="images/640x360-1x-Desktop-TabletLG-Work-JambaJuice-gfx.jpg,
              images/960x540-2x-Desktop-TabletLG-Work-JambaJuice-gfx.jpg 2x">
      <source media="" 
              srcset="images/384x216-1x-TabletSm-Mobile-Work-JambaJuice-gfx.jpg,
              images/576x324-2x-TabletSm-Mobile-Work-JambaJuice-gfx.jpg 2x">
      <img class="img-responsive">
    </picture></a>
</section>