<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail service">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)" srcset="images/Desktop-1920x1080_1x_DremelDepot-Hero-GFX.jpg, images/Desktop-2880x1620_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x438_1x_DremelDepot-Hero-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Hero-GFX.jpg 2x">
            <source media="" srcset="images/Mobile-414x552_1x_DremelDepot-Hero-GFX.jpg, images/Mobile-621x828-2x-WorkDetailHeroGFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div class="title">
            <h2>We focus on<br class="hide-desktop"><strong>Lead Generation, Brand Building, Customer Acquisition & Retention</strong><br><em>-Key areas that drive our clients success.</em></h2>
          </div>
          <div class="hide-desktop no-margin-bottom">
            <h3>Lead Generation</h3>
            <ul>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
            </ul>
            <h3>Brand Building</h3>
            <ul>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
            </ul>
            <h3>Customer Acquisition & Retention</h3>
            <ul>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Lorem ipsum dolor sit amet</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section hide-desktop">
        <picture class="content">
          <source media="(min-width: 1280px)" srcset="images/Desktop-1600x900_1x_DremelDepot-Process-Image2-GFX.jpg, images/Desktop-2160x1215_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 769px)" srcset="images/Tablet-lg-1280x720_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-lg-1920x1080_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="(min-width: 431px)" srcset="images/Tablet-sm-768x432_1x_DremelDepot-Process-Image2-GFX.jpg, images/Tablet-sm-1152x648_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <source media="" srcset="images/Mobile-414x233_1x_DremelDepot-Process-Image2-GFX.jpg, images/Mobile-621x349_2x_DremelDepot-Process-Image2-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
      </section>
      
      
      <div class="title-section">
        <h1>SERVICES</h1>
      </div>

      <section class="detail-section clearfix centered">
        <div class="icon-container">
          <img class="icon" src="images/icon-strategy.png">
          <span class="heading">Strategy</span>
        </div>
        <div class="icon-container">
          <img class="icon" src="images/icon-media.png">
          <span class="heading">Media</span>
        </div>
        <div class="icon-container">
          <img class="icon" src="images/icon-marketing.png">
          <span class="heading">Marketing</span>
        </div>
        <div class="icon-container">
          <img class="icon" src="images/icon-social.png">
          <span class="heading">Social</span>
        </div>
        <div class="icon-container">
          <img class="icon" src="images/icon-production.png">
          <span class="heading">Production</span>
        </div>
        <div class="icon-container">
          <img class="icon" src="images/icon-video.png">
          <span class="heading">Video</span>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-strategy.png">
            <div class="heading"><h2>LACED Strategy</h2></div>
            <p class="caption">The Game Plan: 17+ years experience, combined with a collaboration of creatives, fresh thinkers, and the curious - shaping ideas that make a difference.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>Strategy Services</li>
              <li>Research & Discovery Services</li>
              <li>Strategic Assessment of Current Advertising / Marketing / Sales Systems</li>
              <li>Mapping the Customer Journey from Discovery to Acquisition & Retention</li>
              <li>Strategy & Critical Thinking</li>
              <li>Analysis & Recommendations</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-media.png">
            <div class="heading"><h2>LACED Media</h2></div>
            <p class="caption">Amplify the View: How we help you achieve multi-channel success, capturing the fast moving consumer at scale & on budget.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>Digital Advertising Services</li>
              <li>Media Planning </li>
              <li>Media Buying & Placements</li>
              <li>Media Management - Software & Media Specific Relationships</li>
              <li>Creative Campaigns</li>
              <li>Technical Framework & Media Tracking</li>
              <li>Ongoing Analytics & Reporting</li>
              <li>Media Analysis & Strategic Recommendations</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-marketing.png">
            <div class="heading"><h2>LACED Marketing</h2></div>
            <p class="caption">Pulling your audience in: Delivering Brand resonance that truly matters, one that gains customer investment & long-term loyalty.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>SEO Services - Search Visibility </li>
              <li>Marketing Strategy & Customer Outreach</li>
              <li>Planning & Logistics Rollout</li>
              <li>360 Campaigns (Integrated channel creative)</li>
              <li>Specialized Projects (app builds, micro-sites, initiatives)</li>
              <li>Creative Revamps / Refresh to Current Marketing Mix</li>
              <li>Technical Framework & Tracking</li>
              <li>Ongoing Analytics & Reporting </li>
              <li>Analysis & Recommendations</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-social.png">
            <div class="heading"><h2>LACED Social</h2></div>
            <p class="caption">The way your brand should think and act: influencing your audience and being influenced by them. Social Media can drive your audience to act, buy, and join your brand's tribe.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>Social Strategy & Customer Outreach</li>
              <li>Social Media Planning & Logistics Rollout</li>
              <li>Content Creation & Content Marketing</li>
              <li>Social Media Advertising Campaigns</li>
              <li>Social Promotions/Contests/Creative Campaigns</li>
              <li>Ongoing Social Media Management</li>
              <li>Technical & Tracking Implementation</li>
              <li>Ongoing Social Analytics & Reporting</li>
              <li>Social Media Intelligence Tools and Data Science</li>
              <li>Social Media Analysis & Recommendations</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-production.png">
            <div class="heading"><h2>LACED Production</h2></div>
            <p class="caption">You bring the vision - our talented team of creatives, designers, programmers, and technical engineers make it a reality.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>Ideation & Creative Concepting</li>
              <li>UX - User Experience</li>
              <li>IA - Information Architecture</li>
              <li>Wireframes</li>
              <li>Storyboards</li>
              <li>Design</li>
              <li>Software & App Development</li>
              <li>Programming & Coding</li>
              <li>Technical Framework</li>
              <li>Data Collection & Data Mining</li>
              <li>Digital Asset Management & Inventory</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="item-wrapper clearfix">
          <div class="col-xlg-2 col1">
            <img class="icon centered" src="images/icon-video.png">
            <div class="heading"><h2>LACED Video</h2></div>
            <p class="caption">Award Winning Visuals: Storytelling that resonates - from digital spots, corporate videos, how to’s, edutainment, live video events, & media spots.</p>
          </div>
          <div class="col-xlg-2 col2">
            <h3>Agency Services</h3>
            <ul>
              <li>Ideation & Creative Concepting</li>
              <li>Script Writing: Short Form Storytelling</li>
              <li>Visual Storyboards</li>
              <li>Digital Video Commercial Shoots</li>
              <li>Video Coverage - Live Events</li>
              <li>Video Editing</li>
              <li>Green Screen Shoots</li>
              <li>Motion GraphiX</li>
              <li>VisualFX</li>
              <li>VoiceOver</li>
              <li>Evergreen Content Packaging</li>
            </ul>
          </div>
        </div>
      </section>

      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>