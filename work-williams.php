<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
  <body class="">
    <div class="quick-info">
      <a href="#">Quick Form</a>
    </div>
    <?php include 'header.php'; ?>
    <div class="body-section detail">
      <section id="hero-banner-section">
        <div id="hero-banner">
          <picture class="content">
            <source media="(min-width: 1280px)"
                    srcset="images/Desktop-1980x1080-1x-Williams-HeroBanner-GFX.jpg,
                    images/Desktop-2880x1620-2x-Williams-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 769px)"
                    srcset="images/Tablet-lg-1280x720-1x_Williams-HeroBanner-GFX.jpg,
                    images/Tablet-lg-1920x1080-2x_Williams-HeroBanner-GFX.jpg 2x">
            <source media="(min-width: 431px)"
                    srcset="images/Tablet-sm-768x432-1x_Williams-HeroBanner-GFX.jpg,
                    images/Tablet-sm-1152x648-2x_Williams-HeroBanner-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x55-1x-Williams-HeroBanner-GFX.jpg,
                    images/Mobile-621x828-2x-Williams-HeroBanner-GFX.jpg 2x">
            <img>
          </picture>
        </div>
        <div class="main">
          <div>
            <h2>The Making of Williams Corporate Video</h2>
          </div>
          <p>Until recently, television commercials have been arguably the most powerful way to reach and influence the masses. In recent years this power has largely shifted away from traditional television commercials, and veered towards digital via social media and sharable videos. Williams, a nationwide leader in quality heating, ventilation, and air conditioning (HVAC) products for nearly 100 years, knew it was time to innovate if they wanted to maintain success into the next century.</p>
          <p>They asked LACED Agency to create a digital video that showcased not only Williams products, services, culture, and value–but also could be something shared socially, read by Search Engines easily, and used for multiple initiatives; from advertising, marketing to branding as well as recruiting and educating at tradeshows.</p>
        </div>
      </section>




      <section class="detail-section">
        <picture class="content">
          <source media="(min-width: 1280px)"
                  srcset="images/Desktop-1600x900-1x-Williams-Storyboard-Image1-GFX.jpg,
                  images/Desktop-2160x1215-2x-Williams-Storyboard-Image1-GFX.jpg 2x">
          <source media="(min-width: 769px)"
                  srcset="images/Tablet-lg-1280x720-1x-Williams-Storyboard-Image1-GFX.jpg,
                  images/Tablet-lg-1920x1080-2x-Williams-Storyboard-Image1-GFX.jpg 2x">
          <source media="(min-width: 431px)"
                  srcset="images/Tablet-sm-768x432-1x-Williams-Storyboard-Image1-GFX.jpg,
                  images/Tablet-sm-1152x648-2x-Williams-Storyboard-Image1-GFX.jpg 2x">
          <source media=""
                  srcset="images/Mobile-414x233-1x-Williams-Storyboard-Image1-GFX.jpg,
                  images/Mobile-621x349-2x-Williams-Storyboard-Image1-GFX.jpg 2x">
          <img class="img-responsive">
        </picture>
        <div class="detail-content-wrapper">
          <div>
            <h2>Capturing the Shot</h2>
          </div>
          <p>Working closely with Williams – LACED Agency began writing a script to highlight key selling points while also telling an engaging story. The LACED team designed Storyboards for every key shot and frame. We brought in a talented crew, jibs to capture Williams larger/new factory equipment, green-screen boards for VFX shots, and steady cams & tracks for more complex moving shots. The entire Video Shoot was carefully planned down to the last detail to ensure we met a 2-day turnaround for principle photography. Additionally, we made the decision to use real Williams Employees not actors during the shoot for increased authenticity.</p>
        </div>
      </section>

      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540-1x_Williams-EmployeeShot-Image2-GFX.jpg,
                    images/414-px-1152x648-2x-Williams-EmployeeShot-Image2-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x_Williams-EmployeeShot-Image2-GFX.jpg,
                    images/Mobile-621x349-2x_Williams-EmployeeShot-Image2-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>We made the decision to use real Williams employees vs. actors during the shoot for increased authenticity.</h2>
          </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>Going Pro</h2>
            <p>Taking authenticity one step further, LACED Agency proposed giving viewers a real “Insiders Look” at how Williams makes it’s award-winning, quality products. Using GoPro™ Cameras we set up various product shots to uniquely capture a different perspective of literally “How It’s Made.” Check out the results on the next slide!</p>
          </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1980x1080-1x-Williams-GoPro-Video-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x-Williams-GoPro-Video-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="0" data-video="bBMrwLWVQjo">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>Quality Takes Time</h2>
          </div>
          <p>There’s a reason Williams HVAC Products last decades – they are built to last! As they say, “Quality Takes Time.”</p>
          <p>We wanted to capture this concept visually so we set-up Time-lapse Cameras at various factory stations in order to allow viewers to see work being done over a 1-day period. One of the most exciting time-lapse shots captured was in the Williams warehouse, tracking the shear size and scope of shipments that go in and out of Williams over the course of 1 day.</p>
          <p>Time-lapse shots are not only appealing visually, but also a great way to get more footage than a 2-day shoot physically allows. LACED Agency was able to stretch the budget further, and add more value as an agency partner to Williams.</p>
        </div>
      </section>



      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1980x1080-1x-Williams-TimeLapse-Video-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x-Williams-TimeLapse-Video-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="1" data-video="Q-GoOjgzc68">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
        <div class="detail-content-wrapper">
          <div>
            <h2>Creating A Voice</h2>
          </div>
          <p>Once the shoot was complete, post production and editing began. Part of that process was matching the original script to video captured. The reality is that your script is always a guideline and never an exact match to your video shoot. Successful pre-planning allows for this flexibility. LACED Agency was prepared and quickly went through a series of script revisions to accomodate several new shots (ones we had captured “on the fly” as creativity sparked during the shoot), as well as removing one shot we felt didn’t work with the overall tone of the Williams story.</p>
        </div>
      </section>


      <section class="detail-split-section clearfix">
        <div class="left">
          <picture class="content">
            <source media="(min-width: 431px)"
                    srcset="images/414-px-960x540-1x_Williams-Voice-Image3-GFX.jpg,
                    images/414-px-1152x648_2x_Williams-Voice-Image3-GFX.jpg 2x">
            <source media=""
                    srcset="images/Mobile-414x233-1x_Williams-Voice-Image3-GFX.jpg,
                    images/Mobile-621x349-2x_Williams-Voice-Image3-GFX.jpg 2x">
            <img class="img-responsive">
          </picture>
        </div>
        <div class="right detail-content-wrapper">
          <div>
            <h2>Quickly nearing the finish line, LACED Agency finalized the script and prepared to record voiceover. Talent was enlisted and Voiceover was done in a professional sound studio over the course of 1 day. Click on the “audio icons” to listen to some of the original v/o tracks…</h2>
          </div>
        </div>
      </section>

      <section class="detail-section">
        <div class="detail-content-wrapper no-padding-top padding-80-top">
          <div class="no-margin-bottom">
            <h2>The Final Cut</h2>
            <p>LACED Agency created an animated timeline intro to showcase the 100-year history of the Williams Brand. This was added to the front of the edit. After finalizing the VisualFX, dropping the voiceover tracks in, the Video was edited into sections so that Williams could utilize the video in its’ entirety or as separate stand-alone video segments. Uploaded to YouTube for increased visibility and social share-ability – it was also used at trade-shows, seminars, new business pitches, as well as a soft selling tool playing in the Williams office lobby.</p>
            <p>LACED Agency delivered a digital video that showcased not only Williams products, services, culture, and value–but is also used for multiple initiatives; from advertising, marketing to branding as well as recruiting and educating.</p>
          </div>
      </section>

      <section class="detail-section">
        <div class="player-wrapper">
          <picture class="content">
            <source media="(min-width: 431px)" srcset="images/Youtube-1980x1080-1x-Williams-Master-Video-GFX.jpg">
            <source media="" srcset="images/Youtube-Mobile-621x349-2x-Williams-Master-Video-GFX.jpg">
            <img class="img-responsive">
          </picture>
          <div class="video-wrapper hide">
            <!--insert video player here-->
          </div>
          <div class="play-button youtube-player__play" data-n="2" data-video="e1oyJnwxQp8">
            <svg class="play-button__triangle">
            <use xlink:href="#playtriangle"></use>
            </svg>
          </div>
          <div class="youtube-player__close-btn"></div>
        </div>
      </section>

      <div class="more-work clearfix">
        <h1 class="title-section">MORE WORK</h1>
        <?php include 'more-work.php'; ?>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
      <symbol viewBox="0 0 28 33" id="playtriangle" xmlns:xlink="http://www.w3.org/1999/xlink"> <polygon points="28,16.5 0,33 0,0 "/> </symbol>
      </svg>
      <?php include 'footer.php'; ?>
    </div>
    <?php include 'scripts.php'; ?>
  </body>
</html>